import { mapGetters, mapActions, mapMutations } from 'vuex'
import { Carousel, Slide } from 'vue-carousel'

import winModal from './components/win'
import failModal from './components/fail'

export default {
  name: '',

  data () {
    return {
      show: true,

      myTeam: null,
      enemyTeam: null,
      activeChar: null,
      possibleAttack: null,
      socketWait: false,
      openModal: null,
      canAttack: true,

      deleteBattle: false,

      rewardGold: null,
      rewardExpirience: null,
      myTurn: false
    }
  },

  sockets: {
    'attack-susseful' (result) {
      this.socketAttack(result, 'enemyTeam')
    },

    'not-your-move' () {
      this.socketWait = true
    },

    'enemy-attack' (result) {
      console.log('attack', result)
      this.socketAttack(result, 'myTeam')
    },

    'battle-win' (reward) {
      this.openModal = 'win'
      this.rewardGold = reward.gold
      this.rewardExpirience = reward.expirience
      this.deleteBattle = true
    },

    'enemy-win' (result) {
      this.socketAttack(result, 'myTeam')
      this.openModal = 'fail'
    },

    'move-characters-sus' () {
      this.moveCharacterVisual('myTeam', this.moveTo)
    },

    'move-opponet' (result) {
      // console.log('move', result)
      this.moveCharacterVisual('enemyTeam', result.moveTo)
    },

    'skip-round' () {
      this.skipRound()
    }
  },

  components: {
    Carousel,
    Slide,
    winModal,
    failModal
  },

  computed: {
    ...mapGetters([
      'myTeamForBattle', 
      'enemyTeamForBattle', 
      'battleRound',
      'battleId',
      'user'
    ])
  },



  mounted () {
    this.myTeam = this.calcTeamsPosition(this.myTeamForBattle, true)
    this.enemyTeam = this.calcTeamsPosition(this.enemyTeamForBattle)

    this.getActiveChart()
    this.getPosibbleAttack()

    this.$once('hook:beforeDestroy',  () => {
      this.destroyBattle()
    })
  },

  methods: {
    ...mapActions(['nextRoundItem', 'clearBattle']),
    ...mapMutations(['SET_BATTLE_ROUND']),

    socketAttack(result, team) {
      if(result.newRound) {
        this.SET_BATTLE_ROUND(result.newRound)
      } else {
        this.nextRoundItem()
      }
      this.sound('attack')
      this.updateHealth(result.damage, result.id, team)
      this.socketWait = false
      this.getActiveChart()
      this.getPosibbleAttack()
    },

    getTurn () {
      let id  = this.battleRound[0].id
      let result = false 
      Object.keys(this.myTeam).forEach(key => {
        const val = this.myTeam[key]
        if (val && val.id == id) {
          result = true
        }
      })
      this.myTurn = result
    },

    calcTeamsPosition (val, reverse) {
      let elems = {}
      let elemWitPosition = {}

      if (!val.forEach) {
        val = this.mixArray(val)
      }

      val.forEach(element => {
        if(element.position == null) {
          this.emptys--
        }
        else {
          elemWitPosition[element.position] = element
        }
      })

      if(reverse) {
        for (let index = 1; index <= 10; index++) {
          elems[index] = elemWitPosition[index] || null
        }
      } else {
        for (let index = 10; index >= 1; index--) {
          elems[11 - index] = elemWitPosition[index] || null
        }
      }

      return elems
    },

    mixArray (obj) {
      let array = []
      Object.keys(obj).forEach(key => {
        if(obj[key]) array.push(obj[key])
      })
      return array
    },

    getActiveChart () {

      for (let index = 1; index <= 10; index++) {
        const element = this.myTeam[index]
        if(element && element.id == this.battleRound[0].id) {
          this.activeChar = element
          break
        } else {
          let element2 = this.enemyTeam[index]
          if(element2 && element2.id == this.battleRound[0].id) {
            this.activeChar = element2
            break
          }
        }
      }
      this.getTurn()
      
    },

    getPosibbleAttack () {
      this.possibleAttack = []
      if (this.activeChar.character_slug != 'melee') {
        for (let index = 1; index <= 10; index++) {
          const element =  this.enemyTeam[index]
          element && this.possibleAttack.push(element.id)
        }
      } else if(this.activeChar.position < 6){
        for (let index = 1; index <= 10; index++) {
          const element =  this.enemyTeam[index]
          element && element.position < 6 && this.possibleAttack.push(element.id)
        }

        if(this.possibleAttack.length < 1) {
          for (let index = 1; index <= 10; index++) {
            const element =  this.enemyTeam[index]
            element && this.possibleAttack.push(element.id)
          }
        }
      }
    },

    attackThis (character) {
      if (this.canAttack) {
        this.canAttack = false
        setTimeout( () => {
          this.canAttack = true
          if (this.socketWait) return false
          this.socketWait = true
          this.$socket.emit('attack', {
            targetId: character.id,
            battleId: this.battleId,
            userId: this.user.id
          })
        }, 300)
      }
    },

    updateHealth (damage, id, team) {
      let array = JSON.parse(JSON.stringify(this[team]))
      let i = 0

      Object.keys(array).forEach(key => {
        const val = array[key]
        if(val && val.id == id) {
          let elem = document.getElementById('char' + val.id)
          elem.classList.add('damage')
          setTimeout(() => {
            elem.classList.remove('damage')            
          }, 100)
          if (damage === -1)
            array[key].health = 0
          else 
            array[key].health = array[key].health - damage
          
          if (array[key].health < 1) {
            delete array[key]
            let reverse = false
            if (team == 'myTeam') reverse = true
            array = this.calcTeamsPosition(array, reverse)
          }
        }
        i++
        
      })
      this[team] = array

    },

    moveCharacter (to) {
      if (this.activeChar.team_id != this.user.id) return false 
      this.socketWait = true
      this.moveTo = to
      this.$socket.emit('move-character', {charcterId: this.activeChar.id, moveTo: to, battleId: this.battleId })
    },

    moveCharacterVisual (team, moveTo) {
      let oldPosition
      let canonicalPosition = moveTo
      if (team == 'enemyTeam') {
        oldPosition = 10 - this.activeChar.position + 1
        moveTo = 10 - moveTo + 1
      } else {
        oldPosition = this.activeChar.position
      }
      this.socketWait = false
      let char = null
      if(this[team][moveTo]) {
        char = this[team][moveTo]
      }
      this[team][oldPosition] = char
      this[team][moveTo] = this.activeChar
      this.activeChar.position = Number(canonicalPosition)

      this.nextRoundItem()
      this.getActiveChart()
      this.getPosibbleAttack()
    },

    finishBattle () {
      this.show = false
      this.clearBattle()
      this.$router.go(-1)
    },

    finishBattleFail () {
      this.destroyBattle()

    },

    destroyBattle () {
      if (!this.deleteBattle)  
      this.$http.delete('/api/battles')
    },

    skipRound () {
      this.nextRoundItem()
      this.getActiveChart()
      this.getPosibbleAttack()
    },

    sound (id) {
      document.getElementById(id).play()
    }
  }
}