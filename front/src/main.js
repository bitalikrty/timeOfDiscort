// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from '@/store/index.js'
import VueCarousel from 'vue-carousel'
import {API_URL} from '@/config.json'


import ModalBtn from '@/components/modal/btn.vue'

import VeeValidate from 'vee-validate'

import $http from '@/utils/http'

import VueSocketio from 'vue-socket.io'
import socketio from 'socket.io-client';

const SocketInstance = socketio(API_URL);

Vue.use(VueSocketio, SocketInstance)
Vue.use(VeeValidate)
Vue.use(VueCarousel)
Vue.component('modal-btn', ModalBtn)

Vue.config.productionTip = false

const EventBus = new Vue()

Vue.prototype.$http = $http
Vue.prototype.ea = EventBus
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
