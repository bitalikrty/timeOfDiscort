import VuePerfectScrollbar from 'vue-perfect-scrollbar'
import decor from '@/components/modal-decor'
import Decorator from '@/utils/Decorator'

export default {
  name: '',

  components: {VuePerfectScrollbar, decor},

  data () {
    return {
      showSidebar: false,
      characters: null,
      intervalGet: null,

      possibleCharacters: null,

      priceMin: 0,
      priceMaxP: 0,

      openTab: 'barrack'
    }
  },

  computed: {
    priceMax () {
      if (this.priceMaxP) return this.priceMaxP
      else return 99999999
    }
  },

  methods: { 
    tab (name) {
      this.openTab = name
      this.showSidebar = false
    },

    getCharacters () {
      if (new Date().getTime() > this.intervalGet)
        this.$http.get('/api/town/barrack-characters')
        .then((result) => {
          this.intervalGet = new Date().getTime() + 1000 * 60 * 1
          this.decorCharacters(result.data)
        })
    },

    decorCharacters (characters) {
      let result = []

      for (let index = 0; index < characters.length; index++) {
        const element = characters[index]
        result.push(element || null)
        
      }
      this.characters = result
    },

    hire (characterId) {
      this.$http.get('/api/town/barrack-hire/' + characterId)
      .then(() => {
        this.removeCharacter(characterId)
      })
    },

    removeCharacter (characterId) {
      let newArray = [] 
      Object.keys(this.characters).forEach(key => {
        if (this.characters[key].id != characterId) {
          newArray.push(this.characters[key])
        }  
      })

      this.characters = newArray
    },

    removeChar (characterId) {
      this.$http.delete('/api/town/barrack-remove/' + characterId)
      .then(() => {
        this.removeCharacter(characterId)
      })
    },

    getPossible () {
      this.$http.get('/api/town/barrack-possible')
      .then((result) => {
        this.possibleCharacters = Decorator.decorByProp(result.data, 'barrack_level')
      })
    }
  },

  mounted() {
    this.getPossible()
    
    this.ea.$on('open-Barrack', () => {
      this.getCharacters()
    })

    this.$once('hook:beforeDestroy',  () => {
      this.ea.$off('open-Barrack')
    })
  },
}