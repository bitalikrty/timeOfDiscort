import decor from '@/components/modal-decor'

export default {
  name: 'characterInfo',

  data () {
    return {
      id: null,
      character: null,
      charactersUpgrades: null,
      focusCharacter: null
    }
  },

  components: {decor},

  methods: {
    getCharacters () {
      this.$http.get('/api/characters/full-info/' + this.id)
      .then((result) => {
        this.character = result.data.character
        this.focusCharacter = result.data.character
        this.charactersUpgrades = result.data.characterUpgrades
      })
    },

    upgrade () {
      this.$http.put('/api/characters/update-character/' + this.character.id, {characterLevelSlug: this.focusCharacter.slug})
      .then(() => {
        this.getCharacters()
      })
    },

    getTypeAttack (type) {
      switch (type) {
        case 'physical': return 'Физическая'; break;
        case 'magic': return 'Магическая'; break;
      }
    }
  },

  mounted () {
    this.ea.$on('open-interface', (args) => {
      if (args.name == 'CharcterInfo') {
        this.id = args.id
        this.getCharacters()
      }
    })
  }
}