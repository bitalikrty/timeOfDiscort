import decor from '@/components/modal-decor'
import { mapGetters, mapActions } from 'vuex'

export default {
  name: 'town-interface',

  components: {decor},

  data () {
    return {
      chosenTab: 'info',

      error: {
        mine: null,
        town: null
      }
    }
  },

  computed: {
    ...mapGetters(['myTown', 'user'])
  },

  methods: {
    ...mapActions(['getTown']),
    upgradeMine () {
      this.$http.put('/api/town/my-town/upgrade-mine')
      .then((result) => {
        this.getTown()
      })
      .catch((error) => {

      })
    },
    
    upgradeTown () {
      this.$http.put('/api/town/my-town/upgrade-town')
      .then((result) => {

      })
    },

    calc (number, rounds) {
      console.log(rounds)
      let result = number
      for (let index = 0; index < rounds; index++) {
        result = result * 2
      }
      return result
    }
  },

  mounted () {
    
  }
}