export default {
  data () {
    return {
      typeTravel: null,
      travelTo: null
    }
  },

  methods: {
    startTravel () {
      this.$http.get('/api/travel')
      .then((result) => {
        this.travelTo = result.data
      })
    }
  }
}