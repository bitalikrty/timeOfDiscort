import { mapGetters, mapActions, mapMutations} from 'vuex'

export default {
  name: "",

  sockets: {
    'start-battle' (request) {
      this.SET_MY_TEAM_FOR_BATTLE(request.myTeam)
      this.SET_MY_ENEMY_FOR_BATTLE(request.enemyTeam)
      this.SET_BATTLE_ROUND(request.round.round)
      this.$router.push('/battle/' + this.battleToken)
      this.battleToken = null
    }
  },

  data () {
    return {
      open: false,
      team: null,
      battleToken: null
    }
  },

  computed: {
    ...mapGetters(['myPlace', 'isMyTown', 'user']),
  },

  methods: {
    ...mapMutations([
      'SET_MY_TEAM_FOR_BATTLE', 
      'SET_MY_ENEMY_FOR_BATTLE', 
      'SET_BATTLE_ROUND',
      'SET_BATTLE_ID'
    ]),
    getDistrictTeam () {
      if(this.isMyTown) {
        this.getDistrictTeamQuery(true)
      } else {
        this.getDistrictTeamQuery(false, this.myPlace)
      }
    },

    getDistrictTeamQuery (belonging, townId) {
      let url = belonging ? '/api/town/my-town/district-team' : `api/town/district-team/${townId}`
      this.$http.get(url)
      .then((result) => {
        this.open = true
        this.team = result.data
      })
    },

    attack () {
      this.$http.post('/api/battles/with-enemy', {enemyId: this.team.enemy[0].team_id})
      .then((result) => {
        this.battleToken = result.data
        this.SET_BATTLE_ID(result.data)
        this.$socket.emit('start-battle', {userId: this.user.id ,battleId: result.data});
      })
    },

    getout () {
      this.$http.delete('/api/town/my-town/district-getout')
      .then(() => {
        this.open = false
      })
    }
  },

  mounted () {
    this.ea.$on('open-EnemyDistrict', () => {
      if(this.open == true) {
        this.open = false
      } 
      else {
        this.getDistrictTeam()
      }
      
    })
  }
}