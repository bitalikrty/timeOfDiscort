import { mapGetters, mapActions } from 'vuex'
import decor from '@/components/modal-decor'

export default {
  name: 'partyInterface',

  data () {
    return {
      emptys: 10,
      choosenChar: null,
      characterOnPosition: {}
    }
  },

  components: {decor},  

  computed: {
    ...mapGetters(['myTeam'])
  },
  methods: {
    ...mapActions(['getMyTeam']),
    openCharacterInfo (id) {
      
      // load character info
      // and when load open interface
      this.ea.$emit('open-interface', {name: 'CharcterInfo', id: id})
    },

    calcTeamsPosition (val) {
      this.emptys = 10

      let elemWitPosition = {}

      val.forEach(element => {
        if(element.position == null) {
          this.emptys--
        }
        else {
          elemWitPosition[element.position] = element
        }
      })

      for (let index = 1; index <= 10; index++) {
        this.$set(this.characterOnPosition, index, elemWitPosition[index] || null)
      }

    },

    moveHero (index, characterId) {
      let data = {
        characterId: this.choosenChar.id,
        toMove: index
      }

      this.$http.put('/api/team/move', data)
      .then((result) => {
        this._getMyTeam()
      })
    },

    _getMyTeam () {
      this.ea.$emit('startLoading')
      this.getMyTeam()
      .then(() => {
        this.ea.$emit('endLoading')
        this.calcTeamsPosition(this.myTeam)
      })
    }
  },

  mounted () {

    this.ea.$on('open-PartyInterface', () => {
      this._getMyTeam()
    })
  }
}