import $http from '@/utils/http.js'

const state = {
  town: null,
  districtTeam: null
}

// getters
const getters = {
  myTown: state => state.town,
  myTownDistrictTeam: state => state.districtTeam
}

// actions
const actions = {
  getTown ({commit, state}) {
    return new Promise((resolve, reject) => {
      $http.get('/api/town/my-town').then(
      (response) => {
        commit('SET_TOWN', response.data)
        resolve(true)
      }, (error) => {
        reject(error)
      })
    })
  },

  getMyTownDistrictTeamd ({commit, state}) {
    return new Promise((resolve, reject) => {
      $http.get('/api/town/my-town/district-team').then(
      (response) => {
        commit('SET_DISTRICT_TEAM', response.data)
        resolve(true)
      }, (error) => {
        reject(error)
      })
    })
  }
}

// mutations
const mutations = {
  SET_TOWN (state, town) {
    state.town = town
  },
  
  SET_DISTRICT_TEAM (state, team) {
    state.districtTeam = team
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
