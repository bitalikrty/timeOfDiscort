import $http from '@/utils/http.js'
import { stat } from 'fs';

const state = {
  myPlace: null,
  isMyTown: false,
  socketConnect: false,
  socketId: null
}

// getters
const getters = {
  myPlace: state => state.myPlace,
  isMyTown: state => state.isMyTown,
  socketConnect: state => state.socketConnect,
  socketId: state => state.socketId
}

// actions
const actions = {
  
}

// mutations
const mutations = {
  SET_MY_PLACE (state, townId) {
    state.myPlace = townId
  },

  SET_MY_TOWN (state, isMyTown) {
    state.isMyTown = isMyTown
  },

  SET_CONNECTION (state, id) {
    state.socketConnect = true
    state.socketId = id
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
