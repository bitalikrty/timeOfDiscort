import mongoose from 'mongoose'

if (process.env.NODE_ENV == 'prod') {
  mongoose.connect(process.env.MONGODB_URI, { autoIndex: false })
} else {
  mongoose.connect('mongodb://localhost/timeOfDiscort', { autoIndex: false })
}
let connection = mongoose.connection

connection.collection('rounds').createIndex({battle_id: 1}, {unique: true})

export default mongoose