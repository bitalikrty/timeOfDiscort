import mysql from 'app/utils/mysql'

export default class classesParent{
  constructor (){
    this.mysql = mysql
  }

  $(query) {
		return new Promise( (resolve, reject) => {
			this.mysql.query(query, (err, result) => {
				if (err) {
          console.log(err)
          reject(err)
        }
				else resolve(result)
			}) 
		})
	}
}