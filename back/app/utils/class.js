export default class classesParent{
  constructor (){
    
  }

  $ (actions, args) {
    if(!this[actions]) 
      return false
    if(args)
      return new Promise((resolve, reject) => this[actions](resolve, reject, ...args))
    else 
      return new Promise((resolve, reject) => this[actions](resolve, reject))    
  }
}