import mysql from 'mysql'
import {DataBase} from 'config/config.json'

let db
if (process.env.NODE_ENV == 'prod') {
  db = mysql.createPool(process.env.CLEARDB_DATABASE_URL)
} else {
  db = mysql.createPool({
    host     : DataBase.host || 'localhost',
    port     : DataBase.port || '3306',
    user     : DataBase.user,
    password : DataBase.password,
    database : DataBase.name
  })
}

module.exports = db