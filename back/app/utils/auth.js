import url from 'url'
import signIn from 'app/class/sign/sign.in'

module.exports = (req, res, next) => {
  res.public = {}
  let reqUrl = url.parse(req.url).path.split('/')[1]
  if(reqUrl === 'api') {  
    if (!req.headers.authorization) 
      res.send(401)
    else{ 
      signIn.$('decodeToken', [req.headers.authorization.split(' ')[1]]) 
        .then((result) => {
          res.public.userLogin = result.login
          next()
        })
        .catch((error) => {
          res.send(401)
        })
      }
  } else {
    next()
  }

}