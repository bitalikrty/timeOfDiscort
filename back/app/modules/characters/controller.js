import Characters from 'app/class/characters'
import async from 'async'

export default class {
  constructor(){

  }
  
  getFullInfo(req, res) {
    let id = req.params.id
    let answer = {
      character: null,
      characterUpgrades: null
    }

    Characters.$('getFullInfo', [id])
    .then((result) => {
      answer.character = result
      
      Characters.$('getUpgradesCharacters', [result.slug])
      .then((characterUpgrades) => {
        answer.characterUpgrades = characterUpgrades

        res.send(200, answer)
      })
    })
  }

  updateCharacter (req, res) {
    let id = req.params.id 
    let characterLevelSlug = req.body.characterLevelSlug

    let getCharacterLevel = 
      (next) => Characters.$('getCharacterLevel', [characterLevelSlug])
      .then((result) => {next(null, result)})
    
    let getCharacter = 
      (next) => Characters.$('getCharacter', [id])
      .then((result) => {next(null, result)})
    
    async.parallel([getCharacterLevel, getCharacter], (error, result) => {
      let characterLevel = result[0]
      let character = result[1]
      console.log('character', result)

      if (
        characterLevel.need_experience <= character.experience &&
        characterLevel.inheriot_at == character.slug
      ) {
        Characters.$('updateCharacter', [character.id, characterLevel.id, characterLevel.need_experience])
        .then(() => res.send(200))
        .catch(() => res.send(400))
      } else {
        res.send(400)
      }
    })
    
  }
  
}