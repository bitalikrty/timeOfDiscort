import Controller from './controller'

const controller = new Controller()

module.exports = (app) => {

  app.get('/api/characters/full-info/:id', controller.getFullInfo)
  
  app.put('/api/characters/update-character/:id', controller.updateCharacter)
}


