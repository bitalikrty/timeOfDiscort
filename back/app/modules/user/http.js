import Controller from './controller'

const controller = new Controller()

module.exports = (app) => {
  app.get('/api/user', controller.getMeUser)
}