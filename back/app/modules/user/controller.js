import user from 'app/class/user'

export default class {
  constructor(){

  }

  getMeUser (req, res) {
    user.$('getMeUser', [res.public.userLogin])
    .then((result) => {
      res.send(result)
    })
    .catch((error) => {
      res.send(422)
    })
  }
}