import signUp from 'app/class/sign/sign.up'
import signIn from 'app/class/sign/sign.in'

export default class {
  constructor(){

  }

  signIn(req, res) {
    const payload = {
      login: req.body.login,
      password: req.body.password
    }
    console.log(payload)

    signIn.$('auth', [payload])
    .then((token) => {
      console.log(token)
      res.send(200, token)
    })
    .catch(() => {
      res.send(401)
    })
  }

  signUp(req, res) {
    const user = {
      name: req.body.name,
      email: req.body.email,
      login: req.body.login,
      password: req.body.password
    }
    signIn.$('checkUser',[user.login])
    .then(() => {
      res.send(422)
    })
    .catch((error) => {
      signUp.$('registerUser', [user])
      .then(() => {
        signIn.$('generateToken', [user.login])
        .then((token) => res.send(200, token))
        .catch((error) => {
          console.log(error)
          res.send(400, error || null)
        })
      })
      .catch((error) => {
        console.log(error)
        res.send(400, error || null)
      })
    })
  }
  
}