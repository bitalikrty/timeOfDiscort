import Controller from './controller'

const controller = new Controller()

module.exports = (app) => {
  app.post('/sign-in', controller.signIn)

  app.post('/sign-up', controller.signUp)
}