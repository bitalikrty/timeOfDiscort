import battles from 'app/modules/battles/controller'

let battlesCtrl = new battles()

module.exports = (io) => {
  io.on('connection', (socket) => {

    socket.on('start-battle', (req) => {
      battlesCtrl.$('preLoadForBattle', [req, socket])
      .then((result) => {
        setTimeout(() => {
          battlesCtrl.recursionAttack(req, socket)
        }, 2000)
      })
    })

    socket.on('attack', (req) => {
      
      battlesCtrl.$('attack', [req, socket])
      .then((result) => {
        if(result.win) {
          battlesCtrl.$('getReward',[
            result.winTeamId, 
            result.failTeamId, 
            req.battleId
          ]).then((result) => {
            socket.emit('battle-win', result)
          })
        } else {
          setTimeout(() => {
            battlesCtrl.recursionAttack(req, socket)
          }, 2000)
        }
      })  
      .catch(() => {
        socket.emit('not-your-move')
      })
    })


    socket.on('move-character', (req) => {
      battlesCtrl.$('moveCharacters', [req.charcterId, req.moveTo, req.battleId])
      .then(() => socket.emit('move-characters-sus'))
    })
  })
}