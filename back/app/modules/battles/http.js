import Controller from './controller'

const controller = new Controller()

module.exports = (app) => {

  app.post('/api/battles/with-enemy', controller.initWithEnemy)
  
  app.delete('/api/battles', controller.clearBattle)
  
}