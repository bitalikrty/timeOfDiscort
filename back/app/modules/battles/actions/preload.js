import Core from 'app/utils/class'

/* class */
import Characters from 'app/class/characters'

/* battles class */
import BtlCharacters from 'app/class/battles/characters' 
import BtlBattles from 'app/class/battles/battles' 
import BtlRounds from 'app/class/battles/rounds' 
import Battles from 'app/class/battles'

/* npm */
import async from 'async'

export default class  extends Core{
  constructor(socket, battleId){
    super()
    this.socket = socket
    this.battleId = battleId
    this.characters = []
  }

  go (resolve, reject) {

    this.$('step1Battle').then(() => {
      this.$('step2Team').then(() => {
        this.$('step3').then(() => {
          let result = {
            myTeam: this.team1,
            enemyTeam: this.team2,
            round: this.round
          }
          this.socket.emit('start-battle', result)
          resolve(result)
        })
      })
    })
  }

  step1Battle (resolve, reject) {
    BtlBattles.$('getBattle', [this.battleId])
    .then((battle) => {
      this.battle = battle
      resolve()
    })
    .catch((error) => reject(error))
  }

  step2Team (resolve, reject) {
    let team1 = (next) => {
      Characters.$('getFullTeam', [this.battle.user1_id, false])
      .then((result) => {
        next(null, result)
      })
    }
    let team2 = (next) => {
      Characters.$('getFullTeam', [this.battle.user2_id, this.battle.enemy])
      .then((result) => {
        next(null, result)
      })
    }

    async.parallel([team1, team2], (error, result) => {
      this.team1 = result[0]
      this.team2 = result[1]
      this.characters.push(...this.team1)
      this.characters.push(...this.team2)
      resolve()
    })
  }

  step3 (resolve, reject) {
    let settings = 
      (next) => BtlCharacters.$('startSettingCharacters', [this.characters])
      .then(() => next(null))
      .catch((error) => next(error))
    
    let round = 
      (next) => BtlRounds.$('createRound', [this.characters, this.battle._id])
      .then((result) => next(null, result))
      .catch((error) => next(error))

    async.parallel([settings, round], (error, result) => {
      this.round = result[1]
      resolve()
    })
  }

}

