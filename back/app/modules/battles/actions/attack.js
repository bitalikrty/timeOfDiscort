import Core from 'app/utils/class'


/* battles class */
import BtlCharacters from 'app/class/battles/characters' 
import BtlBattles from 'app/class/battles/battles' 
import BtlRounds from 'app/class/battles/rounds' 
import Battles from 'app/class/battles'

/* npm */
import async from 'async'

export default class  extends Core{
  constructor(socket, battleId, userId, targetId){
    super()
    this.socket = socket
    this.battleId = battleId
    this.userId = userId
    this.targetId = targetId
    this.result = {}
  }

  go (resolve, reject) {
    this.$('step1GetAttacker').then(() =>{
      this.$('step2GetCharacters').then(() => {
        if (!this.characterDefeat.currentHealth || this.characterAttacker.teamId != this.userId) {
          reject()
          return false
        }
        
        this.$('step3Attack').then(() => {
          this.finish()
          resolve(this.result)
        })
      })
    })
  }

  step1GetAttacker (resolve, reject) {
    BtlRounds.$('getAttacker', [this.battleId])
    .then((attackerId) => {
      this.attackerId = attackerId
      resolve()
    })
    .catch((error) => {
      console.log('step1GetAttacker', error)
      reject()
    })
  }
  
  step2GetCharacters (resolve, reject) {
    BtlCharacters.$('getCharacters', [this.attackerId, this.targetId])
    .then((characters) => {
      this.characterAttacker = characters[0]
      this.characterDefeat = characters[1]
      resolve()
    })
    .catch((error) => {
      console.log('step2GetCharacters', error)
      reject()
    })
  }

  step3Attack (resolve, reject) {
    let attack = 
      (next) => BtlCharacters.$('attack', [this.characterAttacker, this.characterDefeat, this.battleId])
      .then((result) => {

        this.result.damage = result.damage || result
        this.result.newRound = result.newRound || null
        this.result.id = this.targetId

        next(null)
      })
      .catch(() => {

        this.result.winTeamId = this.characterAttacker.teamId
        this.result.failTeamId = this.characterDefeat.teamId
        
        next(null)
      })

    let nextRound = 
      (next) => BtlRounds.$('nextRound', [this.battleId])
      .then(() => next())
      .catch(next)
    
    async.parallel([attack, nextRound], (err, result) => {
      if(err) {
        console.log('step3Attack', err)
        reject()
        return false
      } else resolve()
    })
  }

  finish () {
    if(this.result.winTeamId) {
      let result = {
        damage: -1, 
        id: this.targetId, 
        win: true,
        winTeamId: this.result.winTeamId,
        failTeamId: this.result.failTeamId
      }
      this.result = result
    }
    this.socket.emit('attack-susseful', this.result)
  }
}