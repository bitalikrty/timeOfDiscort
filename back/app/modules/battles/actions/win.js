import Core from 'app/utils/class'
import async from 'async'

/* class */
import Power from 'app/class/power'
import Characters from 'app/class/characters'
import User from 'app/class/user'
import Enemy from 'app/class/enemy'


/* battles class */
import BtlCharacters from 'app/class/battles/characters' 
import BtlBattles from 'app/class/battles/battles' 
import BtlRounds from 'app/class/battles/rounds' 
import Battles from 'app/class/battles'

export default class Win extends Core{
  constructor (batleId, teamIdWin, teamIdFail) {
    super()
    this.teamIdFail = teamIdFail
    this.teamIdWin = teamIdWin
    this.batleId = batleId
  }

  go (resolve, reject) {
    this.$('step1Characters')
    .then(() => {
      this.step2Power()

      this.step3Gold()
      this.step3Expirience() 
      this.step3Rating()
      this.$('step4') 
      .then(() => {
        let result = {
          gold: Math.round(this.gold),
          expirience: Math.round(this.expirienceOne)
        }
        resolve(result)
      })
      .catch((error) => {
        console.log(error)
      })
    })
    .catch((error) => {
      console.log(error)
    })
  }

  step1Characters (resolve, reject) {
    let teamWin = 
      (next) => BtlCharacters.$('getCharactersTeam', [this.teamIdWin])
      .then((team) => {
        next(null, team)
      })
      .catch((error) => next(error))
    
    let teamFail =
      (next) => BtlCharacters.$('getCharactersTeam', [this.teamIdFail])
      .then((team) => {
        next(null, team)
      })
      .catch((error) => next(error))
    
    async.parallel([teamWin, teamFail], (errors, result) => {
      this.teamWin  = result[0]
      this.teamFail = result[1]
      resolve()
    })
  }

  step2Power () {

    this.teamWinPower = 0
    this.teamWin.forEach((val) => {
      let item = new Power(val)
      this.teamWinPower += item.get()
    })

    this.teamFailPower = 0
    this.teamFail.forEach((val) => {
      let item = new Power(val)
      this.teamFailPower += item.get()
    })
  }

  step3Gold () {
    this.gold = this.teamFailPower
    if(this.teamFailPower > this.teamWinPower) {
      this.gold += this.teamFailPower - this.teamWinPower
    } else {
      this.gold -= this.teamWinPower - (this.teamFailPower / 2)
      if (this.gold < 1) this.gold = 10 
    }
  }

  step3Expirience () {
    let factor = this.teamFailPower / this.teamWinPower * 100
    let expirience = 150 * factor / 100
    let min = this.teamWin.length * 3
    if(expirience < min) expirience = min 
    
    this.expirienceOne = expirience / this.teamWin.length
  }

  step3Rating () {
    let ratting = 10
    if(this.teamFailPower > this.teamWinPower) {
      ratting += (this.teamFailPower - this.teamWinPower) / 100
    } else {
      ratting -= (this.teamWinPower - this.teamFailPower) / 100
      if (ratting < 1) ratting = 0   
    }
    this.ratting = ratting
  }

  step4 (resolve, reject) {
    let updateCharacters = 
      (next) => this.$('step4UpdateCharacters')
      .then(() => next(null))
      .catch((error) => next(error))
    
    let UpdateUser = 
      (next) => this.$('step4UpdateUser')
      .then(() => next(null))
      .catch((error) => next(error))

    let ClearMonogo = 
      (next) => this.$('step4ClearMonogo')
      .then(() => next(null))
      .catch((error) => next(error))
    
    let removeEnemy = 
      (next) => Enemy.$('removeTeam', [this.teamIdFail])
      .then(() => next(null))
      .catch((error) => next(error))
    
    async.parallel([updateCharacters, UpdateUser, ClearMonogo, removeEnemy], (error) => {
      if(error) reject(error)
      else  resolve()
    })
  }

  step4UpdateCharacters (resolve, reject) {
    let ids = []
    this.teamWin.forEach((val) => {
      if(val.currentHealth > 0) ids.push(val.id)
    })

    Characters.$('addExpirienceToCharacters', [ids, this.expirienceOne])
    .then(resolve)
    .catch(reject)

  }

  step4UpdateUser (resolve, reject) {
    User.$('userWinUpdate', [this.teamIdWin, this.gold, this.ratting])
    .then(resolve)
    .catch(resolve)
  }

  step4ClearMonogo (resolve, result) {
    let clearBattle = 
      (next) => BtlBattles.$('removeBattle', [this.batleId])
      .then(() => next(null))
      .catch((error) => next(error))
    
    let clearRound = 
      (next) => BtlRounds.$('removeRound', [this.batleId])
      .then(() => next(null))
      .catch((error) => next(error))
    
    let clearCharacter1 = 
      (next) => BtlCharacters.$('removeCharacters', [this.teamIdFail])
      .then(() => next(null))
      .catch((error) => next(error))
    
    let clearCharacter2 = 
      (next) => BtlCharacters.$('removeCharacters', [this.teamIdWin])
      .then(() => next(null))
      .catch((error) => next(error))
    
    async.parallel([clearBattle, clearRound, clearCharacter1, clearCharacter2], (error) => {
      if(error) reject(error)
      resolve()
    })
  }
}