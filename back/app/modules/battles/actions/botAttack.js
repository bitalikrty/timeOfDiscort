import Core from 'app/utils/class'


/* battles class */
import BtlCharacters from 'app/class/battles/characters' 
import BtlBattles from 'app/class/battles/battles' 
import BtlRounds from 'app/class/battles/rounds' 
import Battles from 'app/class/battles'

/* npm */
import async from 'async'

export default class  extends Core{
  constructor(socket, battleId, userId){
    super()
    this.socket = socket
    this.battleId = battleId
    this.userId = userId
    this.result = {}
  }

  go (resolve, reject) {
    this.$('step1GetAttacker').then(() => {

      if (this.attacker.character.type_character == 'melee' && this.attacker.position > 5) {
        
        this.$('step2GetTeam').then(() => {

          this.$('step3BotThinkMove')
          .then(() => {
            
            this.$('step4MoveAttacker').then(() => {
              this.resultType = 'move'

              this.$('step5NextRound').then(() => {

                this.finish(resolve)
                
              })

            })

          })
          .catch(() => {
            // IF attacker can't attack and can't move
            this.resultType = 'skip'

            this.$('step5NextRound').then(() => {

              this.finish(resolve)

            })
          })

        })

      } else {
        

        this.$('step2GetOpponentTeam').then(() => {

          this.$('step3BotThinkAttack').then(() => {

            this.step4GetTarget()

            this.$('step5Attack').then(() => {

              this.finish(resolve)

            })

          })

        })

      }
    }) 
  }

  step1GetAttacker (resolve, reject) {
    BtlRounds.$('getAttacker', [this.battleId]).then((attackerId) => {
      this.attackerId = attackerId

      BtlCharacters.$('getCharacter', [attackerId])
      .then((attacker) => {
        this.teamId = attacker.teamId
        this.attacker = attacker
        resolve()
      })
      
    })
  }

  step2GetOpponentTeam (resolve, reject) {
    BtlCharacters.$('getCharactersTeam', [this.userId])
    .then((result) => {
      this.opponentTeam = result
      resolve()
    })
    .catch((error) => {
      reject()
    })
  }

  step2GetTeam (resolve, reject) {
    BtlCharacters.$('getCharactersTeam', [this.teamId])
    .then((result) => {
      this.team = result
      resolve()
    })
    .catch((error) => {
      reject()
    })
  }
  
  step3BotThinkAttack (resolve, reject) {
    Battles.$('botThink', [this.attacker, this.opponentTeam])
    .then((targetId) => {
      this.targetId = targetId
      resolve()
    })
  }

  step3BotThinkMove (resolve, reject) {
    let freePosition = [0, 0, 0, 0, 0]
    this.team.forEach((val) => {
      if(val.position < 6 && val.currentHealth > 0) freePosition[val.position - 1] = 1
    })
    let haveFree = false
    freePosition.forEach((val, i) => {
      if (!val) {
        this.moveTo = i + 1
        haveFree = true
      } 
    })

    if (haveFree) resolve()
    else reject()
  }

  step4GetTarget () {
    this.opponentTeam.forEach(element => {
      if(element.id == this.targetId) {
        this.targetCharacter = element
      }
    })
  }

  step4MoveAttacker (resolve, reject) {
    BtlCharacters.$('moveCharacters', [this.attackerId, this.moveTo])
    .then(() => {resolve()})
    .catch((error) => {
      console.log('step4MoveAttacker', error)
      reject()
    })
  }

  step5NextRound (resolve, reject) {
    BtlRounds.$('nextRound', [this.battleId])
    .then(resolve)
    .catch(reject)
  }

  step5Attack (resolve, reject) {
    let attack = 
      (next) => BtlCharacters.$('attack', [this.attacker, this.targetCharacter, this.battleId])
      .then((result) => {

        this.result.damage = result.damage || result
        this.result.newRound = result.newRound || null
        this.result.id = this.targetId
        this.resultType = 'attack'
        next(null)
      })
      .catch(() => {
        this.result.winTeamId = this.attacker.teamId
        this.result.failTeamId = this.targetCharacter.teamId
        this.resultType = 'win'
        next(null)
      })

    let nextRound = 
      (next) => BtlRounds.$('nextRound', [this.battleId])
      .then(() => next())
      .catch(next)
    
    async.parallel([attack, nextRound], (err, result) => {
      if(err) {
        console.log('step5Attack', err)
        reject()
        return false
      } else resolve()
    })
  }

  finish (resolve) {
    let result
    switch (this.resultType) {
      case 'win': 
        result = {
          damage: -1, 
          id: this.targetId, 
          win: true,
          winTeamId: this.result.winTeamId,
          failTeamId: this.result.failTeamId
        }
        this.socket.emit('enemy-win', result)
        resolve(false)
      break;

      case 'attack':
        this.socket.emit('enemy-attack', this.result)
        resolve(true)
      break;

      case 'move': 
        result = {
          moveTo: this.moveTo
        }
        this.socket.emit('move-opponet', result)
        resolve(true)        
      break;

      case 'skip':
        this.socket.emit('skip-round')
        resolve(true)        
      break;
    }
  }

}