import Core from 'app/utils/class'

/* class */
import User from 'app/class/user'
import Characters from 'app/class/characters'
import Power from 'app/class/power'

/* battles class */
import BtlCharacters from 'app/class/battles/characters' 
import BtlBattles from 'app/class/battles/battles' 
import BtlRounds from 'app/class/battles/rounds' 
import Battles from 'app/class/battles'

/* npm */
import async from 'async'

/* controllers */
import Win from 'app/modules/battles/actions/win'
import Preload from 'app/modules/battles/actions/preload'
import Attack from 'app/modules/battles/actions/attack'
import BotAttack from 'app/modules/battles/actions/botAttack'

export default class  extends Core{
  constructor(){
    super()
  } 

  initWithEnemy (req, res) {
    User.$('getUserId', [res.public.userLogin])
    .then((userId) => {
      let data = [
        userId,
        req.body.enemyId,
        true
      ]
      BtlBattles.$('initBattles', data)
      .then((token) => {
        res.send(200, token)
      })
      .catch((token) => {
        if(token) res.send(200, token)
        else res.send(400)
      })  
    })
  }

  clearBattle (req, res) {
    User.$('getUserId', [res.public.userLogin])
    .then((userId) => {

      BtlBattles.$('getBattleByUser', [userId])
      .then((battle) => {

        if (!battle) {
          res.send(404)
          return false
        }
        let clearBattle = 
          (next) => BtlBattles.$('removeBattle', [battle.id])
          .then(() => next(null))
          .catch((error) => next(error))
        
        let clearRound = 
          (next) => BtlRounds.$('removeRound', [battle.id])
          .then(() => next(null))
          .catch((error) => next(error))
        
        let clearCharacter1 = 
          (next) => BtlCharacters.$('removeCharacters', [battle.user1_id])
          .then(() => next(null))
          .catch((error) => next(error))
        
        let clearCharacter2 = 
          (next) => BtlCharacters.$('removeCharacters', [battle.user2_id])
          .then(() => next(null))
          .catch((error) => next(error))

        
        async.parallel([clearBattle, clearRound, clearCharacter1, clearCharacter2], (err) => {
          if(err)
            console.log('clearBattle', err)
          res.send(200)
        }) 

      })
    })
  }


  preLoadForBattle (resolve, reject, req, socket) {
    new Preload(socket, req.battleId).$('go').then(resolve)
  }

  attack (resolve, reject, req, socket) {

    new Attack(
      socket,
      req.battleId,
      req.userId,
      req.targetId
    ).$('go').then(resolve)

  }

  checkNextAttacker (resolve, reject, req) {
    let userId = req.userId
    async.parallel([
      (next) => {
        BtlRounds.$('getAttacker', [req.battleId]).then((attackerId) => {
          BtlCharacters.$('getCharacter', [attackerId])
          .then((attacker) => {
            next(null, attacker)
          })
          .catch((error) => next(error))
        })
      },
      (next) => {
        BtlBattles.$('getBattle', [req.battleId])
        .then((result) => {
          next(null, result)
        })
        .catch((error) => next(error))
      }
    ], (err, result) => {
      if(err) reject(err)
      else resolve([result[0].teamId != userId, result[1]])
    })
  }


  recursionAttack (req, socket) {
    this.$('checkNextAttacker', [req])
    .then((result) => {
      if(result[0] && result[1].enemy) {
        let attack = new BotAttack(socket, req.battleId, req.userId)

        attack.$('go').then((next) => {
          if (next) setTimeout(() => this.recursionAttack(req, socket), 2000)
        })
      }
    })
  }

  getReward (resolve, reject, teamWinnerId, teamFailId, battleId) {
    let win = new Win(battleId, teamWinnerId, teamFailId)

    win.$('go').then((result) => {
      resolve(result)
    }).catch(reject)
  }
  
  moveCharacters (resolve, reject, characterId, moveTo, battleId) {
    BtlCharacters.$('moveCharacters', [characterId, moveTo])
    .then(() => {
      BtlRounds.$('nextRound', [battleId])
      .then(() => {
        resolve()
      })
      .catch(() => {
        reject()
      })
    })
  }

}