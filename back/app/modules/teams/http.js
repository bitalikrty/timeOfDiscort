import Controller from './controller'

const controller = new Controller()

module.exports = (app) => {

  app.get('/api/team', controller.getTeam)

  app.put('/api/team/move', controller.moveCharacter)

}