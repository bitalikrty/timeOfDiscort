import characters from 'app/class/characters'

export default class {
  constructor(){

  }

  getTeam (req, res) {
    characters.$('getTeam', [res.public.userLogin])
    .then((result) => {
      res.send(result)
    })
  }

  moveCharacter(req, res) {
    let data = {
      toMove: req.body.toMove,
      user: res.public.userLogin,
      characterId: req.body.characterId
    }
    characters.$('moveCharacter', [data])
    .then(() => res.send(200))
    .catch(() => res.send(400))
  }
}