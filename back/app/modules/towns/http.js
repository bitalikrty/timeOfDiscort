import Controller from './controller'

const controller = new Controller()

module.exports = (app) => {

  app.get('/api/town/my-town', controller.getMyTown)

  app.get('/api/town/my-town/district-team', controller.getMyDistrictTeam)

  app.delete('/api/town/my-town/district-getout', controller.getOutDisctrict)

  app.put('/api/town/my-town/upgrade-mine', controller.upgradeMine)

  app.get('/api/town/barrack-characters', controller.getBarrackChar)

  app.get('/api/town/barrack-hire/:id', controller.hireCharacter)

  app.delete('/api/town/barrack-remove/:id', controller.removeCharacter)

  app.get('/api/town/barrack-possible', controller.getPossibleCharacter)

  app.put('/api/town/my-town/upgrade-town', controller.upgradeTown)
}