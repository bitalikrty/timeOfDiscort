import towns from 'app/class/towns'
import enemy from 'app/class/enemy'
import user from 'app/class/user'
import gold from 'app/class/gold'
import barrack from 'app/class/barrack'
import time from 'app/class/time'

import async from 'async'

export default class {
  constructor(){

  }

  getMyTown (req, res) {
    towns.$('getMyTown', [res.public.userLogin])
    .then((result) => {
      res.send(result)
    })
    .catch((error) => {
      res.send(422)
    })
  }


  getMyDistrictTeam (req, res) {
    enemy.$('getTeam', [res.public.userLogin, true])
    .then((result) => {
      res.send(result)
    })
    .catch((error) => {
      console.log(error)
    })
  }

  upgradeMineNext (mine_price, res) {
    let returnGold = 
      (next2) => gold.$('returnUserGold', [res.public.userLogin, mine_price])
      .then(() => next2(null))    

    let upgradeMine = 
      (next2) => towns.$('upgradeMine', [res.public.userLogin])
      .then(() => next2(null))
      
    async.parallel([returnGold], (error, result) => {
      if(error) {
        console.log(error)
        res.send(500)
      } else res.send(200)
    })
  }

  upgradeMine (req, res) {

    // gold.$('returnUserGold', [res.public.userLogin, 20])
    // .then(() => {

    // })
    let getUser = 
      (next) => user.$('getMeUser', [res.public.userLogin])
      .then((user) => {
        next(null, user.gold)
      })          
    
    let getMinePrice = 
      (next) => towns.$('getNextLevelMine', [res.public.userLogin])
      .then((nextMine) => {
        next(null, nextMine)
      })
    
    async.parallel([getUser, getMinePrice], (error, result) => {
      if(error) console.log('upgradeMine', error)
      let haveGold = result[0]
      let mine = result[1]

      if(haveGold >= mine.mine_price) {

        let returnGold = 
          (next2) => gold.$('returnUserGold', [res.public.userLogin, mine.mine_price])
          .then(() => next2(null))    

        let upgradeMine = 
          (next2) => towns.$('upgradeMine', [res.public.userLogin])
          .then(() => next2(null))


        async.parallel([returnGold, upgradeMine], (error, result) => {
          if(error) {
            console.log(error)
            res.send(500)
          } else res.send(200)
        })
      } else {
        res.send(400)
      }
    })
  }

  getBarrackChar (req, res) {
    let getTownId = 
      (next) => towns.$('getMyTown', [res.public.userLogin])
      .then((town) => {
        next(null, town.id)
      })
    let getInterval = 
      (next) => barrack.$('getInterval', [res.public.userLogin])
      .then((result) => {
        next(null, result)
      })
    
    let getChar = 
      (townId) => {
        barrack.$('getCharacters', [townId])
        .then((characters) => {
          res.send(200, characters)
        })
      }

    async.parallel([getTownId, getInterval], (error, result) => {
      let townId = result[0]
      let interval = result[1]
      
      if (!interval) {
        let genereted = 
          (next2) => barrack.$('generetedCharacters', [res.public.userLogin, townId])
          .then(() => next2())
        
        let setInterval = 
          (next2) => barrack.$('setInterval', [townId])
          .then(() => next2())
        
        async.parallel([genereted, setInterval], (error, result) => {
          getChar(townId)
        })
      } else if(time.checkInterval(interval.end)){
        let genereted = 
          (next2) => barrack.$('generetedCharacters', [res.public.userLogin, townId])
          .then(() => next2())
        
        let updateInterval = 
          (next2) => barrack.$('updateInterval', [townId])
          .then(() => next2())
        
        async.parallel([genereted, updateInterval], (error, result) => {
          getChar(townId)
        })
      } else {
        getChar(townId)
      }

    })
  }

  hireCharacter (req, res) {
    let id = req.params.id
    let userLogin = res.public.userLogin

    let getTownId = 
      (next) => towns.$('getMyTown', [userLogin])
      .then((town) => {
        next(null, town.id)
      })

    let getBarrackChar = 
      (next) => barrack.$('getCharacter', [id])
      .then((result) => {
        next(null, result)
      })
    
    let getUser = 
      (next) => user.$('getMeUser', [userLogin])
      .then((result) => next(null, result))
    
    async.parallel([getTownId, getBarrackChar, getUser], (error, result) => {
      let townId = result[0]
      let character = result[1][0]
      let user = result[2]

      let returnGold = 
        (next2) => gold.$('returnUserGold', [userLogin, character.price])
        .then(() => next2())
      
      let hair = 
        (next2) =>  barrack.$('hair', [character, userLogin])
        .then(() => next2())

      if (townId != character.town_id || user.gold < character.price) {
        res.send(400)
        return false
      } else {
        async.parallel([returnGold, hair], () => {
          res.send(200)
        })
      }
    })
  }

  removeCharacter (req, res) {
    let id = req.params.id
    let userLogin = res.public.userLogin

    barrack.$('removeCharacter', [id, userLogin])
    .then((result) => {
      if(result.affectedRows > 0) 
        res.send(200)
      else 
        res.send(400)
    })
    .catch(() => {
      res.send(400)
    })
  }

  getPossibleCharacter (req, res) {
    barrack.$('getPossible', [res.public.userLogin, true])
    .then((result) => {
      res.send(200, result)
    })
    .catch((error) => {
      res.send(400, error)
    })
  }

  getOutDisctrict (req, res) {
    enemy.$('removeTeamByLogin', [res.public.userLogin])
    .then(() => {
      res.send(200)
    })
  }

  upgradeTown (req, res) {
    let getUser = 
      (next) => user.$('getMeUser', [userLogin])
      .then((result) => next(null, result))

    let getTown = 
      (next) => towns.$('getMyTown', [userLogin])
      .then((result) => next(null, result))

    towns.$('upgradeTown') 
    .then(() => {
      res.send(200)
    })
  }
}