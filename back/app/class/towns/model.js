import Model from 'app/utils/model'

const singleton = new class extends Model{
  constructor () {
    super()
    this.table = 'towns'
    this.table_type = 'type_towns'
    this.table_mine = 'mines'
    this.table_barrack = 'barracks'
  }

  checkTown(userLogin) {
    return this.$(`
      SELECT * FROM ${this.table}
      WHERE user_id=(
        SELECT id FROM \`users\` WHERE login='${userLogin}'
      )
    `)
  }

  newTown(userLogin, townTypeSlug) {
    return this.$(`
      INSERT INTO ${this.table}(user_id, type_towns_id, barrack_id, mine_id, level)
      VALUES (
        (SELECT id FROM \`users\` WHERE login='${userLogin}'),
        (SELECT id FROM ${this.table_type} WHERE slug='${townTypeSlug}'),
        (SELECT id FROM ${this.table_barrack} WHERE level=0),
        (SELECT id FROM ${this.table_mine} WHERE level=0),
        1
      )
    `)
  }

  getTown(userLogin) {
    return this.$(`
      SELECT
        ${this.table}.id,
        ${this.table}.level,
        type_towns.gold_bust, 
        type_towns.image, 
        type_towns.name, 
        type_towns.discription, 
        type_towns.rarity, 
        mines.level AS \`mine_level\`,  
        mines.price AS \`mine_price\`,
        mines.gold_per_hour,
        barracks.discount ,
        barracks.price AS \`barrack_price\`,
        barracks.level AS \`barrack_level\`
      FROM ${this.table}
      INNER JOIN ${this.table_type} 
        ON ${this.table}.type_towns_id = ${this.table_type}.id
      INNER JOIN ${this.table_mine} 
        ON ${this.table}.mine_id = ${this.table_mine}.id
      INNER JOIN barracks
        ON ${this.table}.barrack_id = ${this.table_barrack}.id
      WHERE user_id = (SELECT id FROM users WHERE login='${userLogin}')
    `)
  }

  getNextLevelMine(userLogin) {
    return this.$(`
      SELECT 
        ${this.table}.id,
        mines.price AS \`mine_price\`
      FROM ${this.table}
      INNER JOIN ${this.table_mine} 
        ON mines.id = towns.mine_id
      WHERE user_id = (SELECT id FROM users WHERE login='${userLogin}')
    `)
  }

  upgradeMine (userLogin) {
    return this.$(`
      UPDATE towns 
      SET mine_id = mine_id + 1
      WHERE user_id = (SELECT id FROM users WHERE login='${userLogin}')
    `)
  }
  
  upgradeTown (userLogin) {
    return this.$(`
      UPDATE towns 
      SET level = level + 1
      WHERE user_id = (SELECT id FROM users WHERE login='${userLogin}')
    `)
  }
}

export default singleton