import Core from 'app/utils/class'
import model from './model'

const singleton = new class extends Core{
  constructor () {
    super()
  }

  newTown(resolve, reject, userLogin, townType) {
    this.$('checkUserTown', [userLogin])
    .then(()=>{
      reject()
    })
    .catch((result) => {
      if(result == 0) {
        reject() 
        return false
      }

      model.newTown(userLogin, townType)
      .then(() => {
        resolve()
      })
      .catch(reject)
    })
  }

  checkUserTown(resolve, reject, userLogin) {
    model.checkTown(userLogin)
    .then((town) => {
      if(town && town[0]) 
        resolve()
      else 
        reject(1)
    })
    .catch(() => {
      reject(0)
    })
  }

  getMyTown(resolve, reject, userLogin) {
    model.getTown(userLogin)
    .then((result) => {
      let town = result[0]
      resolve(town)
    })
  }

  getNextLevelMine (resolve, reject, userLogin) {
    model.getNextLevelMine(userLogin) 
    .then((result) => {
      resolve(result[0])
    })
  }

  upgradeMine (resolve, reject, userLogin) {
    model.upgradeMine(userLogin)
    .then(resolve)
    .catch(reject)
  }

  upgradeTown (resolve, reject, userLogin) {
    model.upgradeTown(userLogin)
    .then(resolve)
    .catch(reject)
  }
}

export default singleton