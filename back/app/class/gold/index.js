import Core from 'app/utils/class'
import model from './model'

// ch = character
// chs = characters in multiple

const singleton = new class extends Core{
  constructor () {
    super()

  }

  updateGolds (resolve, reject) {
    model.getUsers()
    .then((users) => {
      this.$('calcGold', [users])
      .then((users) => {
        model.updateGold(users)
      })
    })
  }

  calcGold (resolve, reject, users) {
    if(users && users[0]) {
      users.forEach((val, i) => {
        users[i].add_gold = Number(val.gold) + ( Number(val.gold_per_hour) + (val.gold_per_hour / 100) * val.gold_bust)
      })

      resolve(users)
    }
  }

  returnUserGold (resolve, reject, userLogin, gold) {
    model.returnGold(userLogin, gold)
    .then(() => resolve())
    .catch((error) => reject(error))
  }

}

export default singleton