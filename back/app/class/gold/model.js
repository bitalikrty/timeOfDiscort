import Model from 'app/utils/model'

const singleton = new class extends Model{
  constructor () {
    super()
    this.table = 'characters'
    this.table_type = 'character_levels'
  }

  getUsers () {
    return this.$(`
      SELECT 
        users.gold,
        users.id,
        type_towns.gold_bust,
        mines.gold_per_hour
      FROM towns
      LEFT JOIN type_towns
        ON towns.type_towns_id = type_towns.id
      LEFT JOIN users
        ON users.id = towns.user_id
      LEFT JOIN mines
        on towns.mine_id = mines.id
    `)
  }

  updateGold (users) {
    let query = `
      UPDATE users 
      SET gold = 
        CASE id
    `

    users.forEach((val) => {
      query += `WHEN ${val.id} THEN ${val.add_gold} `
    })

    query += ' END WHERE id IN ('

    users.forEach((val, i) => {
      query += `${val.id}`

      if(i + 1 != users.length) {
        query += ', '
      }
    })

    query += ')'

    return this.$(query)
  }

  returnGold (userLogin, gold) {
    return this.$(`
      UPDATE users 
      SET gold = gold - ${gold}
      WHERE login='${userLogin}'
    `)
  }
}

export default singleton