export default class Power {
  constructor (character, sql) {
    this.character = sql ? character : character.character
    this.power = 1
    this.calc()
  }

  calc () {
    this.power += this.character.attack
    this.power += (this.character.healt || this.character.health)  / 5
    this.power += this.character.evasion 
    this.power += this.character.initiative * this.character.attack / 5
    this.power += this.character.critical_hit_chance  
    this.power += this.character.magic_armor
    this.power += this.character.physical_armor
    this.power += (this.character.attack * this.character.theft_life ) / 100
    
  }

  get () {
    return Math.round(this.power)
  }

  
}