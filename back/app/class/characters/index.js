import Core from 'app/utils/class'
import model from './model'

// ch = character
// chs = characters in multiple

const singleton = new class extends Core{
  constructor () {
    super()

  }

  assignChsForUser (resolve, reject, chSlugs, userLogin) {
    model.assignCh(chSlugs, userLogin)
    .then((result) => {
      resolve(result)
    })
    .catch((error) => {
      reject(error)
    })
  }

  getTeam (resolve, reject, userLogin) {
    model.getChsShort(userLogin) 
    .then((result) => {
      resolve(result)
    })
    .catch((error) => {
      reject(error)
    })
  }

  getFullTeam (resolve, reject, id, isEnemy) {
    model.getChs(id, isEnemy) 
    .then((result) => {
      resolve(result)
    })
    .catch((error) => {
      reject(error)
    })
  }

  checkOwner (resolve, reject, user, character) {
    model.checkOwner(user, character) 
    .then((result) => {
      resolve(result)
    })
    .catch(() => {
      reject()
    })
  }

  moveCharacter(resolve, reject, data) {
    this.$('checkOwner', [data.user, data.characterId])
    .then((result) => {
      model.moveCharacter(data.toMove, data.characterId)
      .then(() => {
        resolve()
      })
      .catch((error) => {
        reject()
        console.log(error)
      })
    })
  }

  addExpirienceToCharacters (resolve, reject, ids, expirience) {
    expirience = Math.round(expirience)
    model.updateExprienceS(ids, expirience)
    .then(resolve)
    .catch((error) => {
      console.log('addExpirienceToCharacters', error)
      reject()
    })
  }
  
  getCharacter (resolve, reject, id) {
    model.getCharacter(id)
    .then((result) => {
      resolve(result[0])
    })
    .catch((error) => {
      console.log(error)
      reject(error)
    })
  }

  getFullInfo (resolve, reject, id) {
    model.getFullCharacter(id)
    .then((result) => {
      resolve(result[0])
    })
    .catch((error) => {
      console.log(error)
    })
  }

  getUpgradesCharacters (resolve, reject, characterLevelSlug) {
    model.getChsByLevel (characterLevelSlug) 
    .then((result) => {
      resolve(result)
    })
    .catch((error) => {
      console.log(error)
    })
  }

  getCharacterLevel (resolve, reject, characterLevelSlug) {
    model.getCharacterLevel (characterLevelSlug) 
    .then((result) => {
      resolve(result[0])
    })
    .catch((error) => {
      console.log(error)
    })
  }

  updateCharacter (resolve, reject, id, characterLevelId, experience) {
    model.updateCharacter(id, characterLevelId, experience)
    .then((result) => {
      resolve(result)
    })
    .catch((error) => {
      console.log(error)
    })
  }

  addCharacter (resolve, reject, characterLevel, userLogin) {
    model.addCharacter(characterLevel, userLogin) 
    .then((result) => {
      resolve(result)
    })
    .catch((error) => {
      reject(error)
    })
  }


}

export default singleton