import Model from 'app/utils/model'

const singleton = new class extends Model{
  constructor () {
    super()
    this.table = 'characters'
    this.table_levels = 'character_levels'
    this.groups = 'characters_groups'
  }

  assignCh(slugs, userLogin) {
    let query = `
      INSERT INTO ${this.table}(character_level_id, user_id, experience)
      VALUES
    `

    for (let index = 0; index < slugs.length; index++) {
      const element = slugs[index];
      query += `
        (
          (SELECT id FROM ${this.table_levels} WHERE slug='${element}'), 
          (SELECT id FROM users WHERE login='${userLogin}'),
          0           
        )`
      if (index + 1 < slugs.length) {
        query += ', '
      }
    }
    console.log(query)

    return this.$(query)
  }

  addCharacter (characterLevel, userLogin) {
    return this.$(`
      INSERT INTO ${this.table}(character_level_id, user_id, experience)
      VALUES (${characterLevel}, (SELECT id FROM users WHERE login='${userLogin}'), 0)
    `)
  }
  
  getChsShort (userLogin) {
    return this.$(`
      SELECT 
        ${this.table}.id,
        ${this.table_levels}.name, 
        ${this.table_levels}.img, 
        ${this.table}.position, 
        ${this.table}.experience,
        ${this.table_levels}.full_img
      FROM ${this.table}
      INNER JOIN ${this.table_levels}
      ON ${this.table}.character_level_id = ${this.table_levels}.id
      WHERE user_id = (SELECT id FROM users WHERE login='${userLogin}')
    `)
  }

  checkOwner (userLogin, characterId) {
    return this.$(`
      SELECT * FROM ${this.table} WHERE user_id= (SELECT id FROM users WHERE login='${userLogin}')
    `)
  }

  moveCharacter (toMove, characterId) {
    return this.$(`
      UPDATE ${this.table}
      SET position=${toMove || null}
      WHERE id=${characterId}
    `)
  }

  getChs (userId, isEnemy) {
    let table = isEnemy ? 'enemy_district_character' : 'characters'
    let parentTable = isEnemy ? 'enemy_district_teams' : 'users'
    let character = isEnemy ? 'character_id' : 'character_level_id'
    
    return this.$(`
      SELECT 
        ${table}.position,
        ${table}.id,
        character_levels.attack,
        character_levels.critical_hit_chance,
        character_levels.evasion,
        character_levels.full_img,
        character_levels.health,
        character_levels.health AS ${`max_health`},
        character_levels.img,
        character_levels.initiative,
        character_levels.magic_armor,
        character_levels.name,
        character_levels.number_actions,
        character_levels.physical_armor,
        character_levels.slug,
        character_levels.theft_life,
        character_levels.type_attack,
        types_character.slug AS ${`character_slug`},
        ${isEnemy ? 'enemy_team_id' : 'user_id'} AS ${`team_id`}
      FROM ${table}
      LEFT JOIN character_levels
        ON character_levels.id = ${table}.${character}
      LEFT JOIN types_character
        ON types_character.id = character_levels.type_charater_id
      WHERE 
        ${isEnemy ? 'enemy_team_id' : 'user_id'} = ${userId}
      AND 
        position IS NOT NULL
    `)
  }



  updateExprienceS (ids, experience) {
    let query = `
      UPDATE ${this.table} 
      SET experience = experience + ${experience}
    `

    query += 'WHERE id IN ('

    ids.forEach((val, i) => {
      query += `${val}`

      if(i + 1 != ids.length) {
        query += ', '
      }
    })

    query += ')'

    return this.$(query)
  }

  getFullCharacter (id) {
    return this.$(`
      SELECT 
        ${this.table}.position,
        ${this.table}.id,
        ${this.table}.experience,
        ${this.table_levels}.id AS ${`levelId`},
        ${this.table_levels}.attack,
        ${this.table_levels}.critical_hit_chance,
        ${this.table_levels}.evasion,
        ${this.table_levels}.full_img,
        ${this.table_levels}.health,
        ${this.table_levels}.health AS ${`max_health`},
        ${this.table_levels}.img,
        ${this.table_levels}.initiative,
        ${this.table_levels}.magic_armor,
        ${this.table_levels}.name,
        ${this.table_levels}.number_actions,
        ${this.table_levels}.physical_armor,
        ${this.table_levels}.slug,
        ${this.table_levels}.theft_life,
        ${this.table_levels}.type_attack,
        ${this.table_levels}.need_experience,
        types_character.slug AS ${`character_slug`},
        ${this.groups}.logo AS ${`group_logo`},
        ${this.groups}.name AS ${`group_name`}
      FROM ${this.table}
      INNER JOIN ${this.table_levels}
        ON ${this.table_levels}.id = ${this.table}.character_level_id
      LEFT JOIN types_character
        ON types_character.id = ${this.table_levels}.type_charater_id
      LEFT JOIN ${this.groups} 
        ON ${this.groups}.id = ${this.table_levels}.group
      WHERE 
        ${this.table}.id = ${id} 
      LIMIT 1
    `)
  }

  getChsByLevel (slug) {
    return this.$(`
      SELECT 
        ${this.table_levels}.*,
        ${this.groups}.logo AS ${`group_logo`},
        ${this.groups}.name AS ${`group_name`}
      FROM ${this.table_levels}
      LEFT JOIN ${this.groups} 
        ON ${this.groups}.id = ${this.table_levels}.group
      WHERE inheriot_at = '${slug}'
    `)
  }

  getCharacter (id) {
    return this.$(`
      SELECT 
        ${this.table}.id,
        ${this.table}.experience,
        ${this.table_levels}.slug
      FROM ${this.table}
      INNER JOIN ${this.table_levels}
        ON ${this.table_levels}.id = ${this.table}.character_level_id
      WHERE 
       ${this.table}.id = ${id} 
      LIMIT 1
    `)
  }

  getCharacterLevel (slug) {
    return this.$(`
      SELECT * FROM ${this.table_levels}
      WHERE 
        slug = '${slug}' 
      LIMIT 1
    `)
  }

  updateCharacter (id, levelId, experience) {
    return this.$(`
      UPDATE ${this.table}
      SET 
        experience = experience - ${experience},
        character_level_id = ${levelId}
      WHERE id=${id}
    `)
  }

}

export default singleton