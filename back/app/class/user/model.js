import Model from 'app/utils/model'

const singleton = new class extends Model{
  constructor () {
    super()
    this.table = 'users'
  }

  getUser(login) {
    return this.$(`SELECT * FROM ${this.table} WHERE login='${login}' LIMIT 1`)
  }

  getUserId(login) {
    return this.$(`SELECT id FROM ${this.table} WHERE login='${login}' LIMIT 1`)
  }

  addGold(userId, gold) {
    return this.$(`UPDATE ${this.table} SET gold = gold + ${gold} WHERE id=${userId}`)
  }

  updateGRW (userId, gold, rating) {
    return this.$(`
      UPDATE ${this.table} 
      SET 
        gold = gold + ${gold},
        rating = rating + ${rating},
        wins = wins + 1
      
      WHERE id=${userId}`)
  }

  getRandomUser (login, withTown) {
    return this.$(`
      SELECT 
        ${this.table}.* 
      FROM ${this.table}
      WHERE login <> '${login}'
      ORDER BY RAND()
      LIMIT 1

    `)
  }
}

export default singleton