import Core from 'app/utils/class'
import model from './model'

const singleton = new class extends Core{
  constructor () {
    super()
  }

  getMeUser(resolve, reject, login) {
    model.getUser(login) 
    .then((result) => {
      let user = result[0]
      if(user) {
        delete user.password
        delete user.created_at
        
        resolve(user)
      }else {
        reject()
      }
    })
    .catch((error) => { 
      console.log(error)
      reject(error)
    })
  }

  getUser(resolve, reject, login) {
    model.getUser(login) 
    .then((result) => {
      let user = result[0]
      

      delete user.password
      delete user.created_at
      delete user.gold
      
      resolve(user)
    })
    .catch((error) => {
      console.log(error)
      reject(error)
    })
  }

  getUserId(resolve, reject, login) {
    model.getUserId(login)
    .then((result) => {
      let id = result[0].id
      resolve(id)
    })
    .catch((error) => {
      consoel.log(error)
      reject()
    })
  }

  updateUserGold(resolve, reject, userId, gold) {
    gold = Number(gold)
    userId = Number(userId)
    model.addGold(userId, gold)
    .then(() => {
      resolve()
    })
    .catch((error) => {
      consoel.log(error)
      reject()
    })
  }

  userWinUpdate (resolve, reject, userId, gold, rating) {
    gold = Number(gold)
    userId = Number(userId)
    rating = Number(rating)
    model.updateGRW(userId, gold, rating)
    .then(() => {
      resolve()
    })
    .catch((error) => {
      consoel.log(error)
      reject()
    })
  }

  getRandomUser (resolve, reject, login, withTown) {
    model.getRandomUser(login, withTown)
    .then(() => {
      resolve()
    })
    .catch((error) => {
      consoel.log(error)
      reject()
    })
  }
}

export default singleton