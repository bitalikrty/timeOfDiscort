import Core from 'app/utils/class'

import Battles from 'app/scheme/battles'

import async from 'async'

const singleton = new class extends Core{
  constructor () {
    super()
  } 

  createBattle (resolve, reject, firstId, secondId, token, isEnemy) {
    Battles.find({_id: token}, (err, result) => {
      if(result && result[0]) {
        reject('a')
      } else {
        let battle = new Battles({
          _id: token,
          user1_id: firstId,
          user2_id: secondId,
          enemy: isEnemy
        })
    
        battle.save((error) => {
          if(!error) 
            resolve()
          else
            reject(error) 
        })
      }
    })
  }

  getBattle (resolve, reject, token) {
    Battles.findOne({_id: token}, (err, result) => {
      if(!err && result)
        resolve(result)
      else 
        reject(err)
    })
  }

  getBattleByUser (resolve, reject, userId) {
    Battles.findOne({$or: [
      {user1_id: userId},
      {user2_id: userId}
      ]}, (err, result) => {
      if(!err && result)
        resolve(result)
      else 
        reject(err)
    })
  }

  removeBattle (resolve, reject, battleId) {
    Battles.findOneAndRemove({_id: battleId}, (error) => {
      if(!error) resolve()
      else reject()
    })
  }
}
  
export default singleton
