import Core from 'app/utils/class'

import Characters from 'app/scheme/characters'

import async from 'async'

const singleton = new class extends Core{
  constructor () {
    super()
  } 

  saveCharacter (resolve, reject, characters) {

    async.each(characters, (character, next) => {

      Characters.findOne({id: character.id}, (err, result) => {

        if (!result || result == []) {
          let newCharacter = new Characters({
            id: character.id,
      
            health: character.health,

            position: character.position,
    
            proto_slug: character.slug,

            team_id: character.team_id
          })
    
          newCharacter.save(next)
        } else {
          next()
        }
      })
    }, (err) => {
      if(err) {
        console.log(err)
        reject(err)
      } else resolve()
    })
  }

  getCharacters (resolve, reject, characters) {
    Characters.find(
      {id: {$in: characters}},
      (err, result) => {
        if(!err && result && result[0]) {
          result = this.savingOrderCharacter(characters, result)
          resolve(result)
        }
        else reject(err)
      }
    )
  }

  getCharacter (resolve, reject, characterId) {
    Characters.findOne(
      {id: characterId},
      (err, result) => {
        if(!err && result) resolve(result)
        else reject(err)
      }
    )
  }

  getCharacterByTeamId (resolve, reject, teamdId) {
    Characters.find({team_id: teamdId}, (err, result) => {
      if(!err && result && result[0]) {
        resolve(result)
      }
      else reject(err)
    })
  }

  getCharacterByPosition (resolve, reject, teamdId, position) {
    Characters.findOne({
      team_id: teamdId,
      position: position
    }, (err, result) => {
      if(!err && result) {
        resolve(result)
      }
      else reject(err)
    })
  }

  savingOrderCharacter (ids, characters) {
    let result = []
    let newCharacter = {}
    Object.keys(characters).forEach(key => {
      const val = characters[key]
      newCharacter[val.id] = val
    })
    ids.forEach((val, i) => {
      result[i] = newCharacter[val]
    })

    return result
  }

  removeCharacters (resolve, reject, teamId) {
    Characters.remove({team_id: teamId}, (error) => {
      if(!error) resolve()
      else reject()
    })
  }
}
export default singleton

  