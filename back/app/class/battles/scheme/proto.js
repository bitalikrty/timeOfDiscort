import Core from 'app/utils/class'

import ProtoCharacters from 'app/scheme/protoCharacters'

import async from 'async'

const singleton = new class extends Core{
  constructor () {
    super()
  } 

  getCharacterProto(resolve, reject, characterSlug) {
    ProtoCharacters.findOne({slug: characterSlug}, (err, result) => {
      if(!err && result) 
        resolve(result)
      else 
        reject(err)
    })
  }

  saveCharactersProto(resolve, reject, characters) {
    async.each(characters, (character, next) => {
      let protoCharacter = new ProtoCharacters({
        slug: character.slug,
      
        count: 1,
      
        character: {
          attack: character.attack,
          evasion: character.evasion,
          feature: character.feature,
          full_img: character.full_img,
          healt: character.health,
          img: character.img,
          initiative: character.initiative,
          magic_armor: character.magic_armor,
          physical_armor: character.physical_armor,
          theft_life: character.theft_life,
          type_attack: character.type_attack,
          critical_hit_chance: character.critical_hit_chance,
          type_character: character.character_slug
        }
      })

      protoCharacter.save((err) => {
        if(err) next(err)
        else next()
      })

    }, (err) => {
      if(err) {
        console.log(err)
        reject(err)
      } else resolve()
    })
  }

  getCharactersProto (resolve, reject, slugs) {
    ProtoCharacters.find(
      {slug: {$in: slugs}},
      (err, result) => {
        if(!err && result && result[0]) resolve(result)
        else reject(err)
      }
    )
  }
}
  
export default singleton
