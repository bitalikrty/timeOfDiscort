import Core from 'app/utils/class'

import Rounds from 'app/scheme/rounds'

import async from 'async'

const singleton = new class extends Core{
  constructor () {
    super()
  } 

  saveRound (resolve, reject, round, battleId) {
    this.$('getRound', [battleId])
    .then((round) => {
      resolve(round)
    })
    .catch(() => {
      let _round = new Rounds({
        battle_id: battleId,
        round: round
      })
      
  
      _round.save((err) => {
        if(err) {
          console.log(err)
          reject(err)
        } else {
          resolve(_round)
        } 
  
      })
    })
  }

  getRound(resolve, reject, battleId) {
    Rounds.findOne({battle_id: battleId}, (err, result) => {
      if(!err && result) 
        resolve(result)
      else 
        reject(err)
    })
  }
  
  removeRound (resolve, reject, battleId) {
    Rounds.findOneAndRemove({battle_id: battleId}, (error) => {
      if(!error) resolve()
      else reject()
    })
  }
}
  
export default singleton
