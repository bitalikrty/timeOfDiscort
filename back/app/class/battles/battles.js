import Core from 'app/utils/class'

import battlesScm from './scheme/battles'

const singleton = new class extends Core{
  constructor () {
    super()

  }

  initBattles(resolve, reject, firstId, secondId, isEnemy) {
    let token = this.createBattleId(secondId, firstId)
    battlesScm.$('createBattle', [firstId, secondId, token, isEnemy])
    .then(() => {
      resolve(token)
    })
    .catch((error) => {
      if(error == 'a') reject(token)
      else reject()
    })
  }

  createBattleId (secondId, firstId, isEnemy) {
    return Number( '' + secondId + firstId )
  }

  getBattle (resolve, reject, token) {
    battlesScm.$('getBattle', [token])
    .then((result) => {
      resolve(result)
    })
  }

  getBattleByUser (resolve, reject, userId) {
    battlesScm.$('getBattleByUser', [userId])
    .then((result) => {
      resolve(result)
    })
  }

  removeBattle (resolve, reject, battleId) {
    battlesScm.$('removeBattle', [battleId])
    .then(resolve)
    .catch(reject)
  }

}
export default singleton