import Core from 'app/utils/class'

import Battles from 'app/scheme/battles'
import Rounds from 'app/scheme/rounds'
import ProtoCharacters from 'app/scheme/protoCharacters'
import Characters from 'app/scheme/characters'

import async from 'async'

const singleton = new class extends Core{
  constructor () {
    super()
  } 
  

  // createBattle (resolve, reject, firstId, secondId, token, isEnemy) {
  //   Battles.find({_id: token}, (err, result) => {
  //     if(result && result[0]) {
  //       reject('a')
  //     } else {
  //       let battle = new Battles({
  //         _id: token,
  //         user1_id: firstId,
  //         user2_id: secondId,
  //         enemy: isEnemy
  //       })
    
  //       battle.save((error) => {
  //         if(!error) 
  //           resolve()
  //         else
  //           reject(error) 
  //       })
  //     }
  //   })
  // }

  // getBattle (resolve, reject, token) {
  //   Battles.findOne({_id: token}, (err, result) => {
  //     if(!err && result)
  //       resolve(result)
  //     else 
  //       reject(err)
  //   })
  // }

  // saveRound (resolve, reject, round, battleId) {
  //   this.$('getRound', [battleId])
  //   .then((round) => {
  //     resolve(round)
  //   })
  //   .catch(() => {
  //     let _round = new Rounds({
  //       battle_id: battleId,
  //       round: round
  //     })
      
  
  //     _round.save((err) => {
  //       if(err) {
  //         console.log(err)
  //         reject(err)
  //       } else {
  //         resolve(_round)
  //       } 
  
  //     })
  //   })
  // }

  // getRound(resolve, reject, battleId) {
  //   Rounds.findOne({battle_id: battleId}, (err, result) => {
  //     if(!err && result) 
  //       resolve(result)
  //     else 
  //       reject(err)
  //   })
  // }


  // getCharacterProto(resolve, reject, characterSlug) {
  //   ProtoCharacters.findOne({slug: characterSlug}, (err, result) => {
  //     if(!err && result) 
  //       resolve(result)
  //     else 
  //       reject(err)
  //   })
  // }

  // saveCharactersProto(resolve, reject, characters) {
  //   async.each(characters, (character, next) => {
  //     let protoCharacter = new ProtoCharacters({
  //       slug: character.slug,
      
  //       count: 1,
      
  //       character: {
  //         attack: character.attack,
  //         evasion: character.evasion,
  //         feature: character.feature,
  //         full_img: character.full_img,
  //         healt: character.health,
  //         img: character.img,
  //         initiative: character.initiative,
  //         magic_armor: character.magic_armor,
  //         physical_armor: character.physical_armor,
  //         theft_life: character.theft_life,
  //         type_attack: character.type_attack,
  //         critical_hit_chance: character.critical_hit_chance,
  //         type_character: character.character_slug
  //       }
  //     })

  //     protoCharacter.save((err) => {
  //       if(err) next(err)
  //       else next()
  //     })

  //   }, (err) => {
  //     if(err) {
  //       console.log(err)
  //       reject(err)
  //     } else resolve()
  //   })
  // }


  // saveCharacter (resolve, reject, characters) {

  //   async.each(characters, (character, next) => {

  //     Characters.findOne({id: character.id}, (err, result) => {

  //       if (!result || result == []) {
  //         let newCharacter = new Characters({
  //           id: character.id,
      
  //           health: character.health,

  //           position: character.position,
    
  //           proto_slug: character.slug,

  //           team_id: character.team_id
  //         })
    
  //         newCharacter.save(next)
  //       } else {
  //         next()
  //       }
  //     })
  //   }, (err) => {
  //     if(err) {
  //       console.log(err)
  //       reject(err)
  //     } else resolve()
  //   })
  // }

  // getCharacters (resolve, reject, characters) {
  //   Characters.find(
  //     {id: {$in: characters}},
  //     (err, result) => {
  //       if(!err && result && result[0]) {
  //         result = this.savingOrderCharacter(characters, result)
  //         resolve(result)
  //       }
  //       else reject(err)
  //     }
  //   )
  // }

  // getCharacter (resolve, reject, characterId) {
  //   Characters.findOne(
  //     {id: characterId},
  //     (err, result) => {
  //       if(!err && result) resolve(result)
  //       else reject(err)
  //     }
  //   )
  // }

  // getCharactersProto (resolve, reject, slugs) {
  //   ProtoCharacters.find(
  //     {slug: {$in: slugs}},
  //     (err, result) => {
  //       if(!err && result && result[0]) resolve(result)
  //       else reject(err)
  //     }
  //   )
  // }

  // getCharacterByTeamId (resolve, reject, teamdId) {
  //   Characters.find({team_id: teamdId}, (err, result) => {
  //     if(!err && result && result[0]) {
  //       resolve(result)
  //     }
  //     else reject(err)
  //   })
  // }

  // getCharacterByPosition (resolve, reject, teamdId, position) {
  //   Characters.findOne({
  //     team_id: teamdId,
  //     position: position
  //   }, (err, result) => {
  //     if(!err && result) {
  //       resolve(result)
  //     }
  //     else reject(err)
  //   })
  // }


  // savingOrderCharacter (ids, characters) {
  //   let result = []
  //   let newCharacter = {}
  //   Object.keys(characters).forEach(key => {
  //     const val = characters[key]
  //     newCharacter[val.id] = val
  //   })
  //   ids.forEach((val, i) => {
  //     result[i] = newCharacter[val]
  //   })

  //   return result
  // }

  // removeBattle (resolve, reject, battleId) {
  //   Battles.findOneAndRemove({_id: battleId}, (error) => {
  //     if(!error) resolve()
  //     else reject()
  //   })
  // }

  // removeRound (resolve, reject, battleId) {
  //   Rounds.findOneAndRemove({battle_id: battleId}, (error) => {
  //     if(!error) resolve()
  //     else reject()
  //   })
  // }

  // removeCharacters (resolve, reject, teamId) {
  //   console.log(teamId)
  //   Characters.remove({team_id: teamId}, (error) => {
  //     if(!error) resolve()
  //     else reject()
  //   })
  // }

}

export default singleton