import Core from 'app/utils/class'
import schema from './schema'
import async from 'async'
// subclass 
// import ProtoChar from './subclass/protoChar'
// import Characters from './subclass/character'
// import Attack from './subclass/attack'
import Bot from './subclass/bot'

const singleton = new class extends Core{
  constructor () {
    super()

  }
  botThink (resolve, reject, attacker, team) {
    let bot = new Bot(attacker, team)

    setTimeout(() => {
      let result = bot.get()
      result && resolve(result.id)
      !result && reject()
    })
  }

}

export default singleton