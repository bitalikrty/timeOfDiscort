import Core from 'app/utils/class'

/* subclass */
import ProtoChar from './subclass/protoChar'
import Characters from './subclass/character'
import Attack from './subclass/attack'

/* scheme */
import charactersScm from './scheme/characters'
import protoScm from './scheme/proto'

/* other battle class */
import Rounds from './rounds'

import async from 'async'

const singleton = new class extends Core{
  constructor () {
    super()

  }


  startSettingCharacters (resolve, reject, characters) {

    async.parallel([
      (next) => {
        ProtoChar.$('setProtoCharacters', [characters])
        .then((result) => {
          next(null, result)
        })
      },

      (next) => {
        Characters.$('addCharacters', [characters])
        .then(() => {
          next(null)
        })
      }
    ], (err, result) => {
      resolve()
    })
  }

  getCharacters (resolve, reject, attackedCharacterId, targetCharacterId ) {
    charactersScm.$('getCharacters', [[attackedCharacterId, targetCharacterId]])
    .then((result) => {
      let characters = result
      this.$('getCharactersProto', [[result[0].proto_slug, result[1].proto_slug]])
      .then((charactersProto) => {
        let result2 = Characters.mixCharacterWithProto(characters, charactersProto)
        resolve(result2)
      })
      .catch((err) => {
        console.log(err)
      })
    })
  }

  getCharactersTeam (resolve, reject, teamId) {
    charactersScm.$('getCharacterByTeamId', [teamId])
    .then((characters) => {
      let slugs = []
      characters.forEach((val) => {
        if(! (slugs.indexOf(val.proto_slug) + 1)) 
          slugs.push(val.proto_slug)
      })
      this.$('getCharactersProto', [slugs])
      .then((charactersProto) => {
        let result = Characters.mixCharacterWithProto(characters, charactersProto)
        resolve(result)
      })
    })
  }


  getCharacter (resolve, reject, characterId) {
    charactersScm.$('getCharacter', [characterId])
    .then((character) => {
      this.$('getCharacterProto', [character.proto_slug])
      .then((result) => {
        result.id = characterId
        result.teamId = character.team_id
        result.position = character.position
        resolve(result)
      })
    })
  }

  getCharactersProto (resolve, reject, slugs) {
    protoScm.$('getCharactersProto', [slugs])
    .then((result) => {
      resolve(JSON.parse(JSON.stringify(result)))
    })
  }

  getCharacterProto (resolve, reject, slug) {
    protoScm.$('getCharacterProto', [slug])
    .then((result) => {
      resolve(JSON.parse(JSON.stringify(result)))
    })
  }
  
  attack (resolve, reject, attacker, target, battleId) {
    let attack = new Attack(attacker, target)

    let damage = attack.getResult()
    Characters.$('updatedHealth', [target.id, damage])
    .then(() => {
      this.$('attackDead', [target.id, battleId, target.teamId])
      .then(() => {
        /* TEAM DEAD */
        reject()
      })
      .catch((newRound) => {
        resolve({damage: damage, newRound: newRound})
      })
    })
    .catch(() => resolve(damage))
  }

  attackDead (resolve, reject, characterId, battleId, teamId) {
    let clearRound = 
      (next) => Rounds.$('removeWithRound', [characterId, battleId])
        .then((newRound) => next(null, newRound))
        .catch((error) => next(error))
    
    let checkDeadTeam =
      (next) => this.$('deadCharacters', [teamId])
        .then((teamdDead) => next(null, teamdDead))
        .catch((error) => next(error))
    
    async.parallel([clearRound, checkDeadTeam], (error, result) => {
      // result[0] == new round
      if(result[1]) resolve()
      else reject(result[0])
    })
  }

  removeCharacters (resolve, reject, teamId) {
    charactersScm.$('removeCharacters', [teamId])
    .then(resolve)
    .catch(reject)
  }

  moveCharacters (resolve, reject, characterId, moveTo) {
    charactersScm.$('getCharacter', [characterId])
    .then((result) => {
      let character = result
      let teamId = result.team_id

      charactersScm.$('getCharacterByPosition', [teamId, moveTo])
      .then((characterInThis) => {
        characterInThis.position = Number(character.position)
        character.position = moveTo

        characterInThis.save(() => {
          character.save(() => {
            resolve()
          })
        })
      })
      .catch(() => {
        character.position = moveTo
        character.save(() => {
          resolve()
        })
      })
    })
  }

  deadCharacters (resolve, reject, teamId) {
    charactersScm.$('getCharacterByTeamId', [teamId])
    .then((characters) => {
      let teamLive = false
      characters.forEach((val) => {
        if(val.health > 0) {
          teamLive = true
        }
      })

      resolve(!teamLive)
    })
    .catch((error) => reject(error))
  }

}
export default singleton