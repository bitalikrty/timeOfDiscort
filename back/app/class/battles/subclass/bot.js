import Core from 'app/utils/class'
import async from 'async'


export default class extends Core{
  constructor (attacker, team) {
    super()
    this.attacker = attacker
    this.team = team

    this.clearDeath()
    this.team.length && this.clearByType()
    this.team.length && this.clearByArmor()
    if(this.team.length > 1) {
      this.clearByHealth()
    }

  }

  get () {
    if(this.team.length > 1) {
      return this.team[0]
    } else {
      return this.team
    }
  }

  clearDeath () {
    let result = []
    for (let index = 0; index < this.team.length; index++) {
      const element = this.team[index];
      if (element.currentHealth < 1) {
      } else {
        result.push(element)
      }
    }

    this.team = result
  }

  clearByType () {
    if(this.attacker.character.type_character == 'melee') {
      let array = []
      this.team.forEach((element, i) => {
        if(element.position < 6) {
          array.push(element)
        }  
      })
      if(array.length > 0) 
        this.team = array
    }
  }
  
  clearByArmor () {
    let array = []
    if(this.attacker.character.type_attack == 'physical') {
      this.team.forEach((element, i) => {
        if(element.character.physical_armor < element.character.magic_armor) {
          array.push(element)
        }  
      })
      
      if(!array || !array.length || array.length < 1) {
        array = this.team
        array.sort((a, b) => {
          return a.character.physical_armor > b.character.physical_armor
        })
        this.team = array[0]
      } else {
        this.team = array
      }
    } else {
      this.team.forEach((element, i) => {
        if(element.character.physical_armor > element.character.magic_armor) {
          array.push(element)
        }  
      })

      if(!array || !array.length || array.length < 1) {
        array = this.team
        array.sort((a, b) => {
          return a.character.physical_armor < b.character.physical_armor
        })
        this.team = array[0]
      } else {
        this.team = array
      }
    }
  }

  clearByHealth () {
    this.team.sort((a, b) => {
      return a.character.health > b.character.health
    })
  }
}
