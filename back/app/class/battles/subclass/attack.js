import Core from 'app/utils/class'
import async from 'async'


export default class extends Core{
  constructor (attacker, target) {
    super()
    this.attacker = attacker
    this.target = target
    this.resultAttack = this.attacker.character.attack
    this.typeAttack = this.attacker.character.type_attack
    this.evasion = false
    this.addHealth = 0

    this.startCalc()
    this.calcEvasion()
    this.calcCriticalDamage()
  }

  getResult() {
    if(this.resultAttack < 1) this.resultAttack = 1
    return this.resultAttack
  }

  startCalc () {
    if(this.typeAttack == 'physical') {
      this.resultAttack = this.resultAttack - this.target.character.physical_armor
    } else {
      this.resultAttack = this.resultAttack - this.target.character.magic_armor
    }
  }

  calcEvasion () {
    if( this.getChance(this.attacker.character.evasion) ) {
      this.evasion = true
      this.resultAttack = 0
    }
  }

  calcCriticalDamage () {
    if( this.getChance(this.attacker.character.critical_hit_chance) ) {
      this.resultAttack = this.resultAttack * 2
    }
  }

  getChance(chance) {
    let number = this.randomIntFromInterval(0, 100)

    if(chance > number) {
      console.log('true')
      return true
    } else {
      return false
    }
  }





  randomIntFromInterval(min,max) {
    return Math.floor(Math.random()*(max-min+1)+min);
  }
}
