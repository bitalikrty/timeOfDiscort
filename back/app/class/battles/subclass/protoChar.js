import Core from 'app/utils/class'

import protoScm from '../scheme/proto'

import async from 'async'


const singleton = new class extends Core{
  constructor () {
    super()

  }

  clearCharacters (resolve, reject, characters) {
    let slugs = []
    let result = []
    characters.forEach((val, i) => {
      if(this.find(slugs, val.slug) + 1) {
        
      } else {
        slugs.push(val.slug)
        result.push(val)
      }

      if(i + 1 == characters.length) {
        resolve(result)
      }
    })

  }

  find(array, value) {

    for (var i = 0; i < array.length; i++) {
      if (array[i] == value) return i;
    }
  
    return -1;
  }

  setProtoCharacters (resolve, reject, characters) {
    if(!characters) reject()

    this.$('clearCharacters', [characters])
    .then((result) => {
      let arrayToSave = []
      let slugToCount = []
      
      async.each(result, (val, next) => {
        protoScm.$('getCharacterProto', [val.slug])
          .then((result) => {
            next()
          })
          .catch(() => {
            arrayToSave.push(val)
            next()
          })
        }, () => {
          this.$('saveCharacters', [arrayToSave])
          .then(resolve)
        })
    })
  }
  
  saveCharacters (resolve, reject, characters) {
    protoScm.$('saveCharactersProto', [characters])
    .then(() => { 
      resolve()
    })
  }
}

export default singleton