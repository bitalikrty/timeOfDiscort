import Core from 'app/utils/class'

import charactersScm from '../scheme/characters'

import async from 'async'


const singleton = new class extends Core{
  constructor () {
    super()

  }

  addCharacters (resolve, reject, characters) {
    charactersScm.$('saveCharacter', [characters])
    .then(resolve)
    .catch(reject)
  }

  updatedHealth (resolve, reject, charactersId, attack) {
    charactersScm.$('getCharacter', [charactersId])
    .then((character) => {
      let dead = false

      character.health = character.health - attack
      
      if(character.health < 1) {
        character.health = 0
        dead = true
      }
      character.save((err) => {
        if(err) console.log(err)

        if(dead)
          resolve()
        else 
          reject()
      })
    })
  }

  mixCharacterWithProto (characters, protos) {
    let result = []
    characters = JSON.parse(JSON.stringify(characters))
    protos = JSON.parse(JSON.stringify(protos))
    protos = this.decorProto(protos)
    for (let i = 0; i < characters.length; i++) {
      const val = characters[i];
      result[i] = {}
      for (let key in protos[val.proto_slug]){
        result[i][key]=protos[val.proto_slug][key]
      }
      result[i].id = val.id
      result[i].teamId = val.team_id
      result[i].currentHealth = val.health
      result[i].position = val.position
    }
    
    return result
  }

  decorProto (proto) {
    let result = {}
    proto.forEach((val) => {
      result[val.slug] = val
    })
    return result
  }
}

export default singleton