import Core from 'app/utils/class'

import roundsScm from './scheme/rounds'

const singleton = new class extends Core{
  constructor () {
    super()
  }


  createRound (resolve, reject, allCharacters, battleId) {
    let round = allCharacters
    let roundResult = []

    round.sort( (a, b) => {
      return b.initiative - a.initiative
    })


    round.forEach((val) => {
      roundResult.push({id: val.id, img: val.img})
      if(val.number_actions > 1) {
        for (let index = 0; index < val.number_actions - 1; index++) {
          roundResult.push({id: val.id, img: val.img})
        }
      }
    })
    
    roundsScm.$('saveRound', [roundResult, battleId])
    .then((result) => {
      resolve(result)
    })
  }

  removeWithRound (resolve, reject, characterId, battleId) {  
    roundsScm.$('getRound', [battleId])
    .then((round) => {
      let array = []
      let roundOld = JSON.parse(JSON.stringify(round.round))
      roundOld.forEach((val, i) => {
        if(val.id != characterId)
        { 
          array.push(val)
        }          
      })
      round.round = array

      round.save((err) => {
        if(err) {
          console.log(round)
          console.log(err)
          reject()
        } else {
          resolve(array)
        }
      })

      
    })
    .catch((error) =>{
      console.log('removeWithRound', error)
    })
  }

  nextRound (resolve, reject, battleId) {
    roundsScm.$('getRound', [battleId])
    .then((round) => {
      let array = round.round
      let element = array.shift()
      array.push(element)
      round.round = array 
      round.save((err) => {
        if(err) {
          console.log(err)
          reject()
        } else {
          resolve()
        }
      })
    })
  }

  skipRound (resolve, reject, battleId) {
    
  }

  removeRound (resolve, reject, battleId) {
    roundsScm.$('removeRound', [battleId])
    .then(resolve)
    .catch(reject)
  }

  getAttacker (resolve, reject, battleId) {
    roundsScm.$('getRound', [battleId])
    .then((result) => {
      let attacker = result.round[0].id
      resolve(attacker)
    })
  }
}
export default singleton