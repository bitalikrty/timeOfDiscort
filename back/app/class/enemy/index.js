import Core from 'app/utils/class'
import model from './model'
import decorator from './decorator'
import async from 'async'
import Power from 'app/class/power'


const singleton = new class extends Core{
  constructor () {
    super()

  }


  getTeam (resolve, reject, userLogin, belonging) {
    model.getTeamByLogin(userLogin, belonging)
    .then((result) => {
      if (result && result[0]) {
        let rt = {}
        rt.enemy = result
        rt.power = this.getTeamPower(result)
        resolve(rt)
      } else {
        model.createTeam(userLogin, belonging)
        .then((result) => {

          let teamId = result.insertId

          this.$('generetedEnemyTeam', [userLogin, belonging, teamId])
          .then((team) => {
            console.log(team)
            model.saveEnemys([team])
            .then(() => {
              this.$('getTeam', [userLogin, belonging])
              .then(resolve)
            })

          })
        })
      }
    })
  }

  getTeamPower (team) {
    let teamWinPower = 0
    team.forEach((val) => {
      let item = new Power(val, true)
      teamWinPower += item.get()
    })
    return teamWinPower
  }

  generetedEnemyTeam (resolve, reject, userLogin, belonging, teamId) {
    this.$('getUserTownAndEnemy', [userLogin])
    .then((result) => {
      console.log(result)

      let amountEnemysForTeam = this.randomIntFromInterval(2, 10)
      let team
      this.$('genereteEnemyForTeam', [amountEnemysForTeam, result])
      .then((result) => {
        team = {
          teamId: teamId,
          teamEnemys: result
        }
        resolve(team)
      })

    })
  }



  getUserTownAndEnemy (resolve, reject, userLogin) {
    model.getUserTownAndEnemy(userLogin)
    .then((result) => {
      // console.log(result)
      resolve(result)
    })
    .catch((error) => {
      console.log(error)
    })
  }

  createRandowEnemy (resolve, reject) {
    this.$('getUserTownAndEnemy')
    .then((result) => {

      let beginningResult = result
      decorator.$('decorPosibleEnemy', [result])
      .then((result) => {

        let posiblleEnemys = result

        this.$('createTeam', [beginningResult])
        .then((teams) => {

          this.$('generatedEnemys', [teams, posiblleEnemys])
          .then((result) => {
            model.saveEnemys(result)
            .then(() => {
              resolve()
            })
          })

        })

      })
      // resolve()
    })
    .catch((error) => {
      console.log(error)
    })
  }


  createTeam (resolve, reject, array) {

    async.parallel([
      (next) => {
        model.clearTeam(array).then(() => next(null))
      },
      (next) => {
        decorator.$('separateUserTown', [array]).then((result) => next(null, result))
      }
    ], (error, result) => {
      if(error) 
        console.log(error)
      else 
        console.log('clear')
      
      model.createTeams(result[1])
      .then(() => {
        model.getTeams()
        .then((teams) => resolve(teams))
      })
    })
  }


  generatedEnemys(resolve, reject, teams, posiblleEnemys) {
    let enemys = []
    let length = teams.length

    async.each(teams, (team, next) => {
      const element = team
      
      let enemysForCurrentType = posiblleEnemys[element.type_town_id]
      let amountEnemysForTeam = this.randomIntFromInterval(2, 10)

      this.$('genereteEnemyForTeam', [amountEnemysForTeam, enemysForCurrentType.enemys])
      .then((result) => {
        enemys.push({
          teamId: team.team_id,
          teamEnemys: result
        })
        next(null)
      })

    }, (err, result) => {
      resolve(enemys)
    })
  }


  genereteEnemyForTeam (resolve, reject, amount, enemys) {
    let firstRow = [5,1,4,3,2]
    let secondRow = [6,7,10,9,8]
    let enemyNumers = enemys.length

    async.map([...Array(amount).keys()], (arg1, next) => {

      let newEnemyIndex = this.randomIntFromInterval(0, enemyNumers -1 )
      let newEnemy = {
        enemy: enemys[newEnemyIndex]
      }

      if (newEnemy.enemy.type == 'melee') {
      
        if(firstRow.length > 0) {
          newEnemy.position = firstRow.pop()
        } else {
          newEnemy.position = secondRow.pop()
        }
      }
      else {
        if(secondRow.length > 0) {
          newEnemy.position = secondRow.pop()
        } else {
          newEnemy.position = firstRow.pop()
        }
      }

      next(null, newEnemy)
    }, (err, result) => {
      if(!err) 
        resolve(result)
    })

  }

  removeTeam (resolve, reject, teamId){
    model.removeTeam(teamId)
    .then(resolve)
    .catch((error) => {
      console.log('removeTeam', error)
      reject()
    })
  }

  removeTeamByLogin (resolve, reject, userLogin){
    model.removeTeam(userLogin, true)
    .then(resolve)
    .catch((error) => {
      console.log('removeTeam', error)
      reject()
    })
  }

  randomIntFromInterval(min,max) {
    return Math.floor(Math.random()*(max-min+1)+min);
  }

}

export default singleton