import Core from 'app/utils/class'
import model from './model'


const singleton = new class extends Core{
  constructor () {
    super()
  }

  decorPosibleEnemy (resolve, reject, array) {
    let typeTowns = {}

    array.forEach((val, index) => {
      if(!typeTowns[val.type_town_id])
        typeTowns[val.type_town_id] = {
          enemys: []
        }
      typeTowns[val.type_town_id].enemys.push(
        {
          character_level_id: val.character_level_id,
          min_town_level: val.min_town_level,
          type: val.type_character
        }
      )

      if(index + 1 == array.length) {
        resolve(typeTowns)
      }
    })
  }

  separateUserTown (resolve, reject, array) {
    let userTowns = {}
    array && array.forEach((val, index) => {
      userTowns[val.towns_id] = {id: val.towns_id, level: val.level, type_town_id: val.type_town_id}

      if(index + 1 == array.length) {
        resolve(userTowns)
      }
    })
  }

  
}

export default singleton
