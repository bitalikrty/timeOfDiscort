import Model from 'app/utils/model'

const singleton = new class extends Model{
  constructor () {
    super()
    this.team = 'enemy_district_teams'
    this.posibble = 'district_possible_enemy'
    this.enemy = 'enemy_district_character'
    this.groups = 'characters_groups'
  }

  getTeamByLogin (login, belonging) {
    if (belonging) belonging = 1
    else belonging = 0
    return this.$(`
      SELECT
        character_levels.*,
        ${this.team}.id AS ${`team_id`},
        ${this.groups}.logo AS ${`group_logo`},
        ${this.groups}.name AS ${`group_name`}
      FROM ${this.enemy}
      LEFT JOIN ${this.team}
        ON ${this.team}.id = ${this.enemy}.enemy_team_id
      LEFT JOIN character_levels
       ON ${this.enemy}.character_id = character_levels.id
      LEFT JOIN ${this.groups} 
        ON ${this.groups}.id = character_levels.group
      WHERE 
      ${this.team}.town_id = 
          (SELECT id FROM towns WHERE user_id =
            (SELECT id FROM users WHERE login='${login}')
          )
      AND
        belonging = ${belonging}
          
    `)
  }

  getUserTownAndEnemy (userLogin) {

    let where = (login) => {
      if(login) 
        return `
          WHERE towns.user_id = (SELECT id FROM users WHERE login='${login}')
          AND towns.level >= ${this.posibble}.min_town_level
        `
    }

    return this.$(`
      SELECT 
        towns.level,
        district_possible_enemy.character_level_id,
        district_possible_enemy.min_town_level,
        towns.id AS ${`towns_id`},
        type_towns.id AS ${`type_town_id`},
        character_levels.img AS ${`preview`},
        types_character.slug AS ${`type_character`}
      FROM ${this.posibble}
      LEFT JOIN type_towns
        ON type_towns.id = ${this.posibble}.town_type_id
      LEFT JOIN towns 
        ON type_towns.id = towns.type_towns_id
      LEFT JOIN character_levels
        ON  ${this.posibble}.character_level_id = character_levels.id
      LEFT JOIN types_character 
        ON character_levels.type_charater_id = types_character.id
      ${where(userLogin)}
    `)
  }

  clearTeam () {
    return this.$(`DELETE FROM enemy_district_teams`)
  }
  
  createTeams (towns) {
    let query = `INSERT INTO ${this.team}(town_id, belonging) VALUES `
    let variable = ''
    let index = 0
    let length = Object.keys(towns).length
    
    for (variable in towns) {
      query += `(${towns[variable].id}, 0),`
    }

    for (variable in towns) {
      query += `(${towns[variable].id}, 1) `
      
      if(index + 1 != length) {
        query += ', '
      }
      index++
    }

    return this.$(query)
  }

  createTeam (userLogin, belonging) {
    return this.$(`
      INSERT INTO ${this.team}(town_id, belonging) 
      VALUES (
        (SELECT id FROM towns WHERE user_id =
          (SELECT id FROM users WHERE login='${userLogin}')
        ), 
        ${belonging}
      )
    `)
  }

  getTeams () {
    return this.$(`
      SELECT 
        type_towns.id AS ${`type_town_id`},
        enemy_district_teams.id AS ${`team_id`}
      FROM enemy_district_teams
      LEFT JOIN towns
        ON towns.id = enemy_district_teams.town_id
      LEFT JOIN type_towns
        ON type_towns.id = towns.type_towns_id
    `)
  }


  saveEnemys (enemys) {
    let query = `
      INSERT INTO enemy_district_character(character_id,enemy_team_id, position)
      VALUES 
    `
    enemys.forEach((team, i) => {
      let last = i + 1 != enemys.length
      team.teamEnemys.forEach((enemy, i2, arr) => {
        query += `(${enemy.enemy.character_level_id}, ${team.teamId}, ${enemy.position})`
        if (last)
          query += ', '
        else if(i2 + 1 != team.teamEnemys.length) {
          query += ', '
        }
      })
    })
    console.log(query)
    return this.$(query)
  }

  removeTeam (value, isLogin) {
    if(!isLogin) {
      return this.$(`
        DELETE FROM ${this.team}
        WHERE id = ${value}
      `)
    } else {
      return this.$(`
        DELETE FROM ${this.team}
        WHERE town_id = (SELECT id FROM towns WHERE user_id = (SELECT id FROM users WHERE login = '${value}'))
        AND belonging = 1
      `)
    }
  }
}

export default singleton