import md5 from 'md5'
import async from 'async'

import Core from 'app/utils/class'
import model from './model'

import town from 'app/class/towns'
import characters from 'app/class/characters'

const singleton = new class SignIn extends Core{
  constructor () {
    super()
  }

  registerUser(resolve, reject, user) {
    if (user && user.email && user.name && user.login && user.password) {
      user.password = md5(user.password)
      
      model.registerUser(user)
        .then(() => {
          this.$('afterRegisterActions', [user.login])
          .then(resolve)
          .catch(reject)
        })
        .catch(reject)
    }
    else {
      reject()
    }
  }

  afterRegisterActions(resolve, reject, userLogin) {
    let charactersSlug = ['tramp', 'tramp', 'boowI', 'boowI']
    async.parallel([

      (next) => town.$('newTown', [userLogin, 'village'])
        .then(() => next(null))
        .catch((error) => next(error)),
      
      (next) => characters.$('assignChsForUser', [charactersSlug, userLogin])
        .then(() => next(null))
        .catch((error) => next(error)),
      // here

    ], (error, result) => {
      if(!error) resolve()
      else reject(error)
    })
  }
}

export default singleton