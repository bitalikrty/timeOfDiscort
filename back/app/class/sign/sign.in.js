import Core from 'app/utils/class'
import model from './model'
import jwt from 'jsonwebtoken'
import md5 from 'md5'
import {TokenKey} from 'config/config.json'

const singleton = new class SignIn extends Core{
  constructor () {
    super()
  }
  
  auth (resolve, reject, payload) {
    model.findUser(payload.login) 
    .then((user) => {
      user = user[0]
      if (user.password === md5(payload.password)) {
        this.$('generateToken', [payload.login])
        .then(resolve)
        .catch(reject)
      }
      else {
        reject()
      }
    })
    .catch(reject)
  }

  checkUser (resolve, reject, login) {
    model.findUser(login)
    .then((result) => {
      if(result && result[0])
        resolve()
      else reject()
    })
    .catch(reject)
  }

  generateToken (resolve, reject, login) {

    jwt.sign(
			{
				login: login,
				exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24 * 1000),
			}, 
			TokenKey, 
			(err, token) => {
        if(err) 
          reject(err)
        else
          resolve(token)
      }
		)
  }

  decodeToken (resolve, reject, token) {
    jwt.verify(token, TokenKey, (err, decode) => {
      if(err) 
        reject(err)
      else 
        resolve(decode)
    })
  }
}

export default singleton