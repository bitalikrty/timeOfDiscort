import Model from 'app/utils/model'

const singleton = new class extends Model{
  constructor () {
    super()
    this.table = 'users'
  }

  findUser(login) {
    return this.$(`
      SELECT * FROM ${this.table}
      WHERE login='${login}'
    `)
  }

  registerUser (user) {
    let query = ''
    query += `INSERT INTO ${this.table}` + '(`name`, `login`, `email`, `password`, `gold`, `wins`, `defeats`)'
    query += `
      VALUES (
        '${user.name}',
        '${user.login}',
        '${user.email}',
        '${user.password}',
        0,
        0,
        0
      )`
    return this.$(query)
  }
}

export default singleton