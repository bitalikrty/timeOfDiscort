import Core from 'app/utils/class'

const singleton = new class extends Core{
  constructor () {
    super()
  }

  checkInterval (time) {
    let timeS = new Date(time).getTime()
    let now = new Date().getTime()
    if(timeS <= now) return true
    else return false
  }
}
export default singleton