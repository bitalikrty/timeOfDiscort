import Core from 'app/utils/class'
import model from './model'
import async from 'async'

import Characters from 'app/class/characters'

const singleton = new class extends Core{
  constructor () {
    super()

  }

  getInterval (resolve, reject, userLogin) {
    model.getInterval(userLogin)
    .then((result) => {
      resolve(result[0])
    })
    .catch((error) => {
      console.log('getInterval', error)
    })
  }

  getCharacter (resolve, reject, id) {
    model.getCharacter(id)
    .then(resolve)
    .catch(reject)
  }
  

  getPossible (resolve, reject, userLogin, all) {
    model.getPossible(userLogin, all)
    .then((possible) => {
      resolve(possible)
    })
    .catch(reject)    
  }

  getCharacters (resolve, reject, townId) {
    model.getCharacters(townId)
    .then((result) => {
      resolve(result)
    })
    .catch(reject)    
  }

  generetedCharacters (resolve, reject, userLogin, townId) {
    this.$('getPossible', [userLogin])
    .then((possible) => {
      let numbers = this.random(1, 3)
      let charactersToSave = []
      let possibleNums = possible.length
      for (let index = 0; index < numbers; index++) {
        charactersToSave.push(this.generetCharacter(possible, townId))
      }
      this.$('saveCharacter', [charactersToSave])
      .then(() => {
        resolve()
      })
    })
  }

  generetCharacter (characters, townId) {
    let chance = this.random(0, 100)
    characters = characters.filter((val) => {
      return val.chance >= chance
    })

    let character = characters[this.random(0, characters.length-1)]

    return {
      character_level_id: character.character_level_id,
      town_id: townId,
      price: character.price
    }
  }

  saveCharacter (resolve, reject, characters) {
    model.saveCharacter(characters)
    .then(resolve)
    .catch(reject)
  }
  

  setInterval (resolve, reject, townId) {
    let start = new Date().getTime()
    let hours = this.random(1, 15) * 60 * 60 * 1000
    let end = new Date(start + hours).toISOString().slice(0, 19).replace('T', ' ')
    let startSql = new Date(start).toISOString().slice(0, 19).replace('T', ' ')
    model.saveInterval(townId, startSql, end)
    .then(resolve)
    .catch(reject)   
  }

  random(min,max) {
    return Math.floor(Math.random()*(max-min+1)+min);
  }

  hair (resolve, reject, character, userLogin) {
    let addCharacterUser = 
      (next) => Characters.$('addCharacter', [character.character_level_id, userLogin])
      .then(() => next())
      .catch((error) => next(error))
    
    let removeAvailable =
      (next) => model.removeAvailable(character.id)
      .then(() => next())
      .catch((error) => next(error))

    async.parallel([addCharacterUser, removeAvailable], () => {
      resolve()
    })
  }

  updateInterval (resolve, reject, townId) {
    let hours = this.random(1, 15) * 60 * 60 * 1000
    let end = new Date(new Date().getTime() + hours).toISOString().slice(0, 19).replace('T', ' ')
    model.updateInterval(townId, end)
    .then(resolve)
    .catch(reject)   
  }

  removeCharacter (resolve, reject, id, userLogin) {
    if(!userLogin) return false 
    else 
      model.removeCharacter(id, userLogin)
      .then(resolve)
      .catch(reject)   
  }
}

export default singleton