import Model from 'app/utils/model'

const singleton = new class extends Model{
  constructor () {
    super()
    this.posible = 'possible_character_in_barrack'
    this.available = 'available_character_in_barrack'
    this.interval = 'barrack_intervals'
    this.levels = 'character_levels'

    this.townType = 'type_towns'
    
    this.townId = (userLogin) => {
      return `(SELECT id FROM towns WHERE user_id = ${this.userId(userLogin)})`
    }

    this.userId = userLogin => {
      return `(SELECT id FROM users WHERE login='${userLogin}')`
    } 

    this.getTownType = (table) => {
      return `INNER JOIN ${this.townType} ON ${this.townType}.id = ${table}`
    }
  }

  getInterval (userLogin) {
    return this.$(`
      SELECT * FROM ${this.interval} WHERE town_id=${this.townId(userLogin)} LIMIT 1
    `)
  }

  getPossible (userLogin, all) {
    let getAll = (all) => {
      if (!all) return `AND towns.level >= ${this.posible}.barrack_level`
      else return ''
    }
    return this.$(`
      SELECT 
        ${this.posible}.*,
        ${this.levels}.img,
        ${this.levels}.name
      FROM ${this.posible}

      ${this.getTownType(this.posible+'.town_type_id')}
      
      LEFT JOIN towns 
      ON 
        towns.type_towns_id = ${this.townType}.id
      ${getAll(all)}
      
      LEFT JOIN ${this.levels}
        ON ${this.levels}.id = ${this.posible}.character_level_id

      WHERE towns.user_id = ${this.userId(userLogin)}
    `)
  }

  saveCharacter (characters) {
    let query = `
      INSERT INTO ${this.available}(character_level_id, town_id, price)
      VALUES 
    `
    characters.forEach((item, i) => {
        query += `(${item.character_level_id}, ${item.town_id}, ${item.price})`
        if(i + 1 != characters.length) {
          query += ', '
        }
    })
    return this.$(query)
  }

  getCharacters (townId) {
    return this.$(`
      SELECT 
        ${this.available}.id,
        ${this.levels}.img,
        ${this.levels}.name,
        ${this.available}.price
      FROM ${this.available}
      INNER JOIN ${this.levels}
        ON ${this.levels}.id = ${this.available}.character_level_id
      WHERE town_id = ${townId}
    `)
  }

  saveInterval (townId, start, end){
    return this.$(`
      INSERT INTO ${this.interval}(town_id, start, end) 
      VALUES (${townId}, '${start}', '${end}')
    `)
  }

  updateInterval (townId, end) {
    return this.$(`
      UPDATE ${this.interval}
      SET end='${end}'
      WHERE town_id = ${townId}
    `)
  }

  getCharacter (id) {
    return this.$(`
      SELECT * FROM ${this.available} WHERE id = ${id}
    `)
  }

  removeAvailable (id) {
    return this.$(`
      DELETE FROM ${this.available}
      WHERE id = ${id}
    `)
  }

  removeCharacter (id, userLogin) {
    return this.$(`
      DELETE FROM ${this.available}
      WHERE 
        id = ${id}
      AND 
        town_id = ${this.townId(userLogin)} LIMIT 1
    `)
  }
}


export default singleton