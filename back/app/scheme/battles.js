import mongoose from 'app/utils/mongodb'
let Schema = mongoose.Schema

let battlesSchema = new Schema({
  _id: Number,
  user1_id: {
    type: Number,
    required: true
  },
  user2_id: {
    type: Number,
    required: true
  },
  enemy: Boolean
})


let Battles = mongoose.model('battles', battlesSchema)

export default Battles

// mongoose.battles.createIndex({_id: 1})