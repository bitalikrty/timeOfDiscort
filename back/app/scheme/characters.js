import mongoose from 'app/utils/mongodb'
let Schema = mongoose.Schema

let characterSchema = new Schema({
  id: {
    type: Number
  },

  position: {
    type: Number,
    min: 1,
    max: 10
  },
  
  health: Number,

  proto_slug: String,

  team_id: Number
})


let character = mongoose.model('characters', characterSchema)

// db.character.createIndex( { "id": 1 }, { unique: true } )

export default character