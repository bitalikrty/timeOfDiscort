import mongoose from 'app/utils/mongodb'
let Schema = mongoose.Schema

let protoCharactersSchema = new Schema({
  slug: {
    type: String,
    required: true
  },

  count: Number,

  character: {
    attack: Number,
    evasion: Number,
    feature: String,
    full_img: String,
    healt: Number,
    img: String,
    initiative: Number,
    magic_armor: Number,
    physical_armor: Number,
    theft_life: Number,
    type_attack: String,
    critical_hit_chance: Number,
    type_character: String
  }
})

let protoCharacters = mongoose.model('protocharacters', protoCharactersSchema)

export default protoCharacters