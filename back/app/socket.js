import battles from 'app/modules/battles/socket'


// let battlesCtrl = new battles()

module.exports = (io) => {
  io.on('connection', function(socket) {
    socket.emit('connection', socket.id)
  })

  battles(io)
}
