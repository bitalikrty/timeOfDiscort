if (process.env.NODE_PATH == 'prod') {
  module.exports = require('./prod.json')
} else {
  module.exports = require('./dev.json')  
}