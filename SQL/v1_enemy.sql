

INSERT INTO types_character(`name`,`slug`)
  VALUES
    ('Ближний бой', 'melee'),
    ('Дальний - стрелковый', 'distance_rifle'),
    ('Дальний - магический', 'distance_magic');
  
INSERT INTO 
  character_levels(
    `name`, 
    `slug`,
    `full_img`, 
    `img`, 
    `attack`, 
    `health`,
    `type_attack`, 
    `feature`, 
    `physical_armor`, 
    `magic_armor`, 
    `critical_hit_chance`,
    `evasion`,
    `initiative`,
    `theft_life`, 
    `number_actions`,
    `inheriot_at`,
    `type_charater_id`
  )
VALUES
  (
    'Рядовой лучник',
    'boow',
    '/character/boow/1.jpg',
    '/character/boow/thumbnail.jpg',
    3,
    15,
    'physical',
    null,
    1,
    1,
    1,
    5,
    10,
    0,
    2,
    0,
    (SELECT id FROM types_character WHERE slug='distance_rifle')
  )
    
