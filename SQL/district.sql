INSERT INTO `district_possible_enemy` ( `character_level_id`, `min_town_level`, `town_type_id`) 
VALUES 
    ((SELECT id FROM character_levels WHERE slug='tramp'), 0, (SELECT id FROM type_towns WHERE slug='village') ),

    ((SELECT id FROM character_levels WHERE slug='boowI'), 1, (SELECT id FROM type_towns WHERE slug='village') ),

    ((SELECT id FROM character_levels WHERE slug='warriorI'), 2, (SELECT id FROM type_towns WHERE slug='village') ),

    ((SELECT id FROM character_levels WHERE slug='warriorII'), 4, (SELECT id FROM type_towns WHERE slug='village') ),

    ((SELECT id FROM character_levels WHERE slug='boowII'), 4, (SELECT id FROM type_towns WHERE slug='village') ),

    ((SELECT id FROM character_levels WHERE slug='boowIII'), 6, (SELECT id FROM type_towns WHERE slug='village') ),

    ((SELECT id FROM character_levels WHERE slug='boowIII'), 6, (SELECT id FROM type_towns WHERE slug='village') ),

    ((SELECT id FROM character_levels WHERE slug='HiredArcherI'), 9, (SELECT id FROM type_towns WHERE slug='village') ),

    ((SELECT id FROM character_levels WHERE slug='SwordMaster'), 9, (SELECT id FROM type_towns WHERE slug='village') ),

    ((SELECT id FROM character_levels WHERE slug='Paladin'), 9, (SELECT id FROM type_towns WHERE slug='village') ),

    ((SELECT id FROM character_levels WHERE slug='SniperI'), 10, (SELECT id FROM type_towns WHERE slug='village') )