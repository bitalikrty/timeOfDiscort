INSERT INTO `possible_character_in_barrack` 
    ( `barrack_level`, `character_level_id`, `price`, `town_type_id`, `chance`) 
VALUES 
    (2,
        (SELECT id FROM character_levels WHERE slug="warriorI"), 
    80,  
        (SELECT id FROM type_towns WHERE slug="village"),
    50
    ),
    (1,
        (SELECT id FROM character_levels WHERE slug="tramp"), 
    30,  
        (SELECT id FROM type_towns WHERE slug="village"),
    100
    ),
    (2,
        (SELECT id FROM character_levels WHERE slug="boowI"), 
    100,  
        (SELECT id FROM type_towns WHERE slug="village"),
    50
    ),

    (4,
        (SELECT id FROM character_levels WHERE slug="tramp2"), 
    300,  
        (SELECT id FROM type_towns WHERE slug="village"),
    50
    ),
    (4,
        (SELECT id FROM character_levels WHERE slug="warriorII"), 
    500,  
        (SELECT id FROM type_towns WHERE slug="village"),
    50
    ),

    (6,
        (SELECT id FROM character_levels WHERE slug="warriorIII"), 
    800,  
        (SELECT id FROM type_towns WHERE slug="village"),
    30
    ),

    (6,
        (SELECT id FROM character_levels WHERE slug="boowII"), 
    800,  
        (SELECT id FROM type_towns WHERE slug="village"),
    30
    ),

    (8,
        (SELECT id FROM character_levels WHERE slug="tramp3"), 
    1500,  
        (SELECT id FROM type_towns WHERE slug="village"),
    30
    ),
    (8,
        (SELECT id FROM character_levels WHERE slug="boowIII"), 
    3000,  
        (SELECT id FROM type_towns WHERE slug="village"),
    30
    ),
    (8,
        (SELECT id FROM character_levels WHERE slug="SwordMaster"), 
    3000,  
        (SELECT id FROM type_towns WHERE slug="village"),
    30
    ),

    (10,
        (SELECT id FROM character_levels WHERE slug="HiredArcherI"), 
    4000,  
        (SELECT id FROM type_towns WHERE slug="village"),
    50
    ),

    (10,
        (SELECT id FROM character_levels WHERE slug="HiredArcherII"), 
    5000,  
        (SELECT id FROM type_towns WHERE slug="village"),
    20
    ),

    (10,
        (SELECT id FROM character_levels WHERE slug="Paladin"), 
    7000,  
        (SELECT id FROM type_towns WHERE slug="village"),
    20
    )
    
    