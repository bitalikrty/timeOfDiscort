INSERT INTO mines(`level`, `gold_per_hour`, `price`) 
  VALUES
    (0, 0, 0), 
    (1, 15, 20),
    (2, 30, 150),
    (3, 60, 300),
    (4, 120, 600),
    (5, 300, 1000),
    (6, 600, 3000),
    (7, 1500, 8000),
    (8, 3000, 30000),
    (9, 10000, 200000),
    (10, 25000, 500000);
  

INSERT INTO barracks(`level`, `discount`, `price`) 
  VALUES
    (0, 0, 0), 
    (1, 1, 20),
    (2, 2, 150),
    (3, 3, 300),
    (4, 5, 600),
    (5, 10, 1000),
    (6, 13, 3000),
    (7, 15, 8000),
    (8, 20, 30000),
    (9, 25, 200000),
    (10, 40, 500000);
  
INSERT INTO type_towns(`name`,`slug`, `rarity`, `image`, `chance_find`, `gold_bust`)
  VALUES
    ('Поселок', 'village', 'Обычный', '/town/village.jpg', 1000, 0),
    ('Город', 'town', 'Обычный', '/town/town.jpg', 1000, 25),
    ('Асгард', 'asgard', 'Дрвений город', '/town/asgard.jpg', 50, 50),
    ('Остров Авалон', 'avalon', 'Мифический', '/town/avalon.jpg', 5, 250);
    