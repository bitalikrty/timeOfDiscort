INSERT INTO district_possible_enemy(`character_level_id`, `town_type_id`, `min_town_level`) 
  VALUES
    (
      (SELECT id FROM character_levels WHERE slug='tramp'), 
      (SELECT id FROM type_towns WHERE slug='village'), 
      1
    ),

    (
      (SELECT id FROM character_levels WHERE slug='boowI'), 
      (SELECT id FROM type_towns WHERE slug='village'), 
      2
    )