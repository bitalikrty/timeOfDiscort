CREATE TABLE IF NOT EXISTS `users` (
  `id` INT AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `login` VARCHAR(255) NOT NULL UNIQUE,
  `email` VARCHAR(255) UNIQUE,
  `gold` INT,
  `password` VARCHAR(255),
  `wins` INT,
  `defeats` INT,
  `rating` INT, 
  `created_at` DATETIME DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY(id)
) CHARACTER SET utf8 COLLATE utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `types_character`(
  `id` INT AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `slug` VARCHAR(255) NOT NULL UNIQUE,
  `created_at` DATETIME DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
) CHARACTER SET utf8 COLLATE utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `characters_groups`(
  `id` INT AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `slug` VARCHAR(255) NOT NULL UNIQUE,
  `logo` VARCHAR(255) NOT NULL,
  `created_at` DATETIME DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
) CHARACTER SET utf8 COLLATE utf8_unicode_ci;



CREATE TABLE IF NOT EXISTS `character_levels` (
  `id` INT AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `slug` VARCHAR(255) NOT NULL UNIQUE,
  `full_img` VARCHAR(255) NOT NULL,
  `img` VARCHAR(255) NOT NULL,
  `health` INT NOT NULL,
  `attack` INT,
  `type_charater_id` INT,
  `feature` INT,
  `need_experience` INT,
  `type_attack` ENUM('magic', 'physical') NOT NULL,
  `physical_armor` INT,
  `magic_armor` INT,
  `critical_hit_chance` INT,
  `evasion` INT,
  `initiative` INT,
  `theft_life` INT,
  `number_actions` INT,
  `inheriot_at` VARCHAR(255),
  `group` INT,
  `created_at` DATETIME DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  FOREIGN KEY (type_charater_id)
    REFERENCES types_character(id) 
    ON DELETE CASCADE
) CHARACTER SET utf8 COLLATE utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `characters` (
  `id` INT AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `character_level_id` INT NOT NULL,
  `position` INT UNIQUE,
  `experience` INT,
  `created_at` DATETIME DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY(id),
  FOREIGN KEY (user_id)
    REFERENCES users(id) 
    ON DELETE CASCADE,
  FOREIGN KEY (character_level_id)
    REFERENCES character_levels(id) 
    ON DELETE CASCADE
) CHARACTER SET utf8 COLLATE utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `mines` (
  `id` INT AUTO_INCREMENT,
  `level` INT,
  `gold_per_hour` INT,
  `price` INT,
  `created_at` DATETIME DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY(id)
) CHARACTER SET utf8 COLLATE utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `barracks` (
  `id` INT AUTO_INCREMENT,
  `level` INT,
  `discount` INT,
  `price` INT,
  `created_at` DATETIME DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY(id)
) CHARACTER SET utf8 COLLATE utf8_unicode_ci;



CREATE TABLE IF NOT EXISTS `type_towns` (
  `id` INT AUTO_INCREMENT,
  `name` VARCHAR(255),
  `slug` VARCHAR(255) NOT NULL UNIQUE,
  `discription` TEXT,
  `rarity` VARCHAR(255),
  `image` VARCHAR(255),
  `chance_find` INT,
  `gold_bust` INT,
  `created_at` DATETIME DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY(id)
)CHARACTER SET utf8 COLLATE utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `towns` (
  `id` INT AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `mine_id` INT NOT NULL, 
  `type_towns_id` INT NOT NULL,
  `barrack_id` INT NOT NULL,
  `level` INT,
  `created_at` DATETIME DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY(id),

  FOREIGN KEY (user_id)
    REFERENCES users(id) 
    ON DELETE CASCADE,
  
  FOREIGN KEY (mine_id)
    REFERENCES mines(id) ,
  
  FOREIGN KEY (barrack_id)
    REFERENCES barracks(id) ,
  
  FOREIGN KEY (type_towns_id)
    REFERENCES type_towns(id) 
    ON DELETE CASCADE

) CHARACTER SET utf8 COLLATE utf8_unicode_ci; 

CREATE TABLE IF NOT EXISTS `district_possible_enemy` (
  `id` INT AUTO_INCREMENT,
  `character_level_id` INT NOT NULL,
  `town_type_id` INT NOT NULL,
  `min_town_level` INT,

  PRIMARY KEY(id),

  FOREIGN KEY (character_level_id)
    REFERENCES character_levels(id) 
    ON DELETE CASCADE,

  FOREIGN KEY (town_type_id)
    REFERENCES type_towns(id) 
    ON DELETE CASCADE

) CHARACTER SET utf8 COLLATE utf8_unicode_ci; 

CREATE TABLE IF NOT EXISTS `enemy_district_teams` (
  `id` INT AUTO_INCREMENT,
  `town_id` INT NOT NULL,
  `belonging` INT,
  `number_of` INT,
  `preview` VARCHAR(255),

  PRIMARY KEY(id),

  FOREIGN KEY (town_id)
    REFERENCES towns(id) 
    ON DELETE CASCADE
) CHARACTER SET utf8 COLLATE utf8_unicode_ci; 


CREATE TABLE IF NOT EXISTS `enemy_district_character` (
  `id` INT AUTO_INCREMENT,
  `enemy_team_id` INT NOT NULL,
  `character_id` INT,
  `position` INT,

  PRIMARY KEY(id),

  FOREIGN KEY (enemy_team_id)
    REFERENCES enemy_district_teams(id) 
    ON DELETE CASCADE,
  
  FOREIGN KEY (character_id)
    REFERENCES character_levels(id)
    ON DELETE CASCADE
) CHARACTER SET utf8 COLLATE utf8_unicode_ci; 


CREATE TABLE IF NOT EXISTS `available_character_in_barrack` (
  `id` INT AUTO_INCREMENT,
  `character_level_id` INT,
  `town_id` INT,
  `price` INT,  

  PRIMARY KEY(id),

  FOREIGN KEY (town_id)
    REFERENCES towns(id)
    ON DELETE CASCADE,
  
  FOREIGN KEY (character_level_id)
    REFERENCES character_levels(id)
    ON DELETE CASCADE
) CHARACTER SET utf8 COLLATE utf8_unicode_ci; 


CREATE TABLE IF NOT EXISTS `possible_character_in_barrack` (
  `id` INT AUTO_INCREMENT,
  `character_level_id` INT,
  `town_type_id` INT,
  `barrack_level` INT,
  `price` INT,
  `chance` INT,

  PRIMARY KEY(id),

  FOREIGN KEY (town_type_id)
    REFERENCES type_towns(id)
    ON DELETE CASCADE,
  
  FOREIGN KEY (character_level_id)
    REFERENCES character_levels(id)
    ON DELETE CASCADE
) CHARACTER SET utf8 COLLATE utf8_unicode_ci; 


CREATE TABLE IF NOT EXISTS `barrack_intervals` (
  `id` INT AUTO_INCREMENT,
  `start` DATETIME DEFAULT CURRENT_TIMESTAMP,
  `end` DATETIME,
  `town_id` INT UNIQUE,

  PRIMARY KEY(id),

  FOREIGN KEY (town_id)
    REFERENCES towns(id)
    ON DELETE CASCADE
) CHARACTER SET utf8 COLLATE utf8_unicode_ci; 